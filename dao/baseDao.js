var db;
var ObjectID = require('mongodb').ObjectID;
var dbFactory = require("./daoConfig");
var model = require('../model');
var appUtil = require('../appUtil/appUtil');

var _callback = function(callback){
	return function(err, res, raw){
		if(err){
            console.log(err);
            throw appUtil.exceptions.DB_ERROR();
		}else{
			if(callback){
				callback(res, raw);
			}
		}
	}
};

var basicOperations = function(collname){
	this.insert = function (json, callback){
		getColl(collname).insert(json, _callback(callback));
	};
	this.remove = function (_id, callback){
		getColl(collname).remove({_id: getId(_id)}, _callback(callback));
	};
	this.update = function (_id, json, callback){
		getColl(collname).update({_id:getId(_id)}, json, _callback(callback))
	};
	this.find = function (_id, callback){
		getColl(collname).findOne({_id: getId(_id)}, _callback(callback));
	}
};

function getColl(name){
	return dbFactory.getdb().collection(name);
};

var getId = function(_id){
    try {
        if (_id._bsontype && _id._bsontype === 'ObjectID') {
            return _id;
        } else {
            return new ObjectID(_id);
        }
    }
    catch(e) {
        throw appUtil.exceptions.INVALID_ID();
    }
};

var generateId = function(){
	return new ObjectID();
};

exports.getColl = getColl;
exports.basicOperations = basicOperations;
exports._callback = _callback;
exports.getId = getId;
exports.generateId = generateId;