var userDao = require("./userDao");
var postDao = require("./postDao");
var winnerDao = require("./winnerDao");
var transactionDao = require("./transactionDao");
var advertisementDao = require("./advertisementDao");
module.exports = {
    userDao : userDao,
    postDao : postDao,
    winnerDao : winnerDao,
    transactionDao : transactionDao,
    advertisementDao : advertisementDao
};