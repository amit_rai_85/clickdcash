var MongoClient = require('mongodb').MongoClient;
var db;

exports.getdb = function(){
	return db;
};
exports.init = function(appName, callback){
	// Initialize connection once
	MongoClient.connect("mongodb://localhost:27017/"+appName, function(err, database) {
	  db = database;
	  console.log("connected to db " + appName);
	  if(err) throw err;
	  callback(database);
	});
};