/**
 * Created by amit on 6/11/14.
 */
 // FIXME : Only required fields shall be returned by dao
var COUNT_MAX = 200;
var fieldsToShow = {loc : false, cmt: false, rpt: false, area: false,
    invu : false, pst : false, likeClicked : false, likedBy : false, commentBy: false };
var baseDao = require("./baseDao");
var model = require("../model");
var postColl = baseDao.getColl('post');
var appUtil = require('../appUtil/appUtil');
var archiveColl = baseDao.getColl('archive');

postColl.ensureIndex({ "uid": 1 }, function (err, res) {
    console.log("ensuring index for uid in post coll .", "err", err, "res", res);
});
postColl.ensureIndex({ "net": 1 }, function (err, res) {
    console.log("ensuring index for city in post coll .", "err", err, "res", res);
});

module.exports = {
    basicOperations: new baseDao.basicOperations('post'),
    deletePost : function(pid, uid, callback){
        postColl.remove({_id : baseDao.getId(pid), uid : uid }, baseDao._callback(function(res){
            callback(res);
        }));
    },

    getPostDetail : function(pid, callback){
        postColl.find({ _id : baseDao.getId(pid) }, fieldsToShow).toArray(baseDao._callback(callback));
    },
    
    findPostbyUid : function (uid, callback) {
        postColl.find({ uid : uid, isan: model.db.IS_ANONYMOUS.NO }, fieldsToShow).toArray(baseDao._callback(callback));
    },

    getPost : function(uid, lat, lng, city, startIn, count, getRecent, pId, direction, isMyPost, user, callback, area){
        var query  = {};
        var options =  {};
        options.sort = [];
        if(city && !isMyPost){
            query.net = city;
            if(area)
                query.area = area;
        }

        if(!startIn){
            startIn = 0;
        }

        if(!count || count==0){
            count = COUNT_MAX;
        }

        if(uid){
            query.uid = uid;
        }

        if(pId){
            if(direction && (direction!="up" && direction!="down" )){
                throw appUtil.exceptions.INVALID_DIRECTION_PARAMETER("PostId");
            }else{
                if(direction==="up"){
                    query._id = {"$gt" : baseDao.getId(pId)};
                }else if(direction==="down"){
                    query._id = {"$lt" : baseDao.getId(pId)};
                }else{
                    query._id = baseDao.getId(pId);
                }
            }
        }

        if(getRecent == true){
            options.sort.push(["_id","desc"]);
        }else{
            options.sort.push(["_id","asc"]);
        }
        var fields = {loc : false, cmt: false, rpt: false, area: false,
            invu : false, pst : false, likeClicked : false, commentBy: false }
        postColl.find(query, fields, options).skip(startIn).limit(count).toArray(baseDao._callback(function(posts){
            for(var i in posts){
                if(posts[i].likedBy && posts[i].likedBy.indexOf(user._id) >=0){
                    posts[i].isLiked = true;
                }else{
                    posts[i].isLiked = false;
                }
                delete posts[i].likedBy;
            }
            callback(posts);
        }));
    },

    votePost : function (pid, uid, ts, callback){
        var update, query;
            postColl.findOne({ _id: baseDao.getId(pid), likedBy: { $in: [uid] }}, {_id: true, likeClicked: true}, baseDao._callback(function (pst) {
                var isLiked = 0;
                if (pst) {
                    update = { $set:{uo : ts}, $inc : { lcnt: -1 } , $pull : { likedBy: uid, invu: uid } };
                } else {
                    isLiked = 1;
                    update = { $set:{uo : ts}, $inc : { lcnt : 1 } , $addToSet : { likedBy: uid, invu: uid, likeClicked: uid } };
                } 
                query = { _id : baseDao.getId(pid) };
                postColl.update(query, update,{fields: {uid: true, likedBy: true}}, baseDao._callback(function (resp) {
                    if (callback) callback(pst, isLiked);
                }));
            }));
    },

    addComment: function (id, msg, uid, ts, callback){
        var arrPos = [];
        var path = "pst";
        var queryPath = null;
        var updatePath = null;
        queryPath = "pst";
        updatePath = "pst";
        var queryObj = { _id : baseDao.getId(id), isd: {$ne: true}};
        var updateObj = { $addToSet : { commentBy: uid, invu: uid }, $set : {uo: ts}, $inc :{ccnt : 1}};
        var xc = updateObj["$push"] = {};
        var newId = baseDao.generateId().toString();
        xc[updatePath] = { msg : msg , isd : false , uid : uid, ts: ts, cid: newId };

        postColl.findAndModify(queryObj ,{}, updateObj,{new: true, fields:{ uid: true, commentBy: true}}, baseDao._callback(function(res){
            if(res){
                callback({ts: ts, cid: newId, uid: res.uid, commentBy: res.commentBy});
            }else{
                throw appUtil.exceptions.INVALID_POST_PARAMETERS();
            }
        }));
    },

    deleteComment: function (id, uid, cid, callback) {
        var arrPos = [];
        var count = 0;
        var path = "pst";
        var queryObj = {};
        queryObj["_id"] = baseDao.getId(id);
        queryObj["isd"] = {$ne: true};
        queryObj[path] = {$elemMatch: {cid: cid}};

        var orCondition = [];
        orCondition.push({ uid : uid });
        var condition1={};
        condition1[path] = {$elemMatch: {uid: uid}};
        orCondition.push(condition1);
        queryObj["$or"] = orCondition;

        var obj = {};
        obj[path + ".$.isd"] = true;
        var update = { $set : obj, $inc :{ccnt : -1} };
        postColl.update(queryObj , update , baseDao._callback(function(res){
            if(res>0){
                callback(res);
            }else{
                throw appUtil.exceptions.INVALID_POST_PARAMETERS();
            }
        }));
    },

    getPostImages: function(id, callback) {
        postColl.find({_id: baseDao.getId(id)}, {vUrl: true, pUrl: true}).toArray(baseDao._callback(callback));
    },

    addReport : function (uid, rptType, pid, callback) {
        var query = { _id : baseDao.getId(pid), isd: false };
        var obj = { $set : { } };
        var rpt =  new model.db.Report(uid, rptType, appUtil.getCurrentUTCTime());
        postColl.findOne(query,{rpt : true, vUrl: true, pUrl:true, msg: true}, baseDao._callback(function(post){
            if(post){
                var newObj = [];
                if(post.rpt) {
                    for (var i in post.rpt) {
                        if (post.rpt[i].uid != uid) {
                            newObj.push(post.rpt[i]);
                        }else{
                            throw appUtil.exceptions.ALREADY_REPORTED();
                            break;
                        }
                    }
                }
                newObj.push(rpt);
                obj["$set"]["rpt"] = newObj;
                postColl.update(query, obj, baseDao._callback(function(res){
                    if(res)
                        callback(post);
                    else
                        callback(null);
                }));
            }else{
                throw appUtil.exceptions.POST_DELETED();
            }
        }));
    },

    IsInTrending : function(pid, callback){
        var query  = {};
        var options =  {};
        options.sort = [];
        var fromtime = appUtil.getCurrentUTCTime() - appUtil.appConstants.TrendingScreenStartDate;
        query.uo = {$gte: fromtime};
        query.isan = model.db.IS_ANONYMOUS.NO;
        options.sort.push(["pop","desc"]);
        options.sort.push(["_id","asc"]);
        postColl.findOne({ _id : baseDao.getId(pid), isd: false }, {_id: false, net: true}, baseDao._callback(function(post){
            query.net = post.net;
            postColl.find(query, {_id: true}, options).skip(0).limit(20).toArray(baseDao._callback(function(posts){
                var isPostExists = false;
                for(var i in posts){
                    if(posts[i]._id == pid){
                        isPostExists = true;
                        break;
                    }
                }
                if(isPostExists)
                    callback(true);
                else
                    callback(false);
            }));
        }));
    },

    getComments : function(pid, callback){
        postColl.findOne({ _id : baseDao.getId(pid) }, {pst: true}, baseDao._callback(callback));
    },

    pushIntoArchived : function(callback){
        var ts = appUtil.getCurrentUTCTime();
        postColl.find({ts: {$lt: ts} }).toArray(baseDao._callback(function(posts){
            for(var i in posts){
                archiveColl.insert(posts[i], baseDao._callback(function(res){}));
            }
            postColl.remove({ ts: {$lt: ts} }, baseDao._callback(function(res){
                if(callback)
                    callback(res);
            }));
        }));
    }
}