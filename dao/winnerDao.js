/**
 * Created by amit on 6/15/15.
 */
// FIXME : Only required fields shall be returned by dao
var model = require("../model/model");
var baseDao = require("./baseDao");
var winnerColl = baseDao.getColl('winner');
var appUtil = require('../appUtil/appUtil');
winnerColl.ensureIndex( { "date": 1 }, { sparse: true }, function(err, res){
    console.log("ensuring sparse index for date in winner coll.","err",err,"res",res);
});

module.exports = {
    basicOperations: new baseDao.basicOperations('winner'),
    getWinners: function (date, callback) {
        var options = {}, query = {};
        options.sort = [];
        options.sort.push(["date","desc"]);
        if(date){
            query["date"] = {$in : date};
        }
        winnerColl.find(query).toArray(baseDao._callback(function(users){
            callback(users);
        }));
    }
}