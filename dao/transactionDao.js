/**
 * Created by amit on 6/15/15.
 */
// FIXME : Only required fields shall be returned by dao
var model = require("../model/model");
var baseDao = require("./baseDao");
var transactionColl = baseDao.getColl('transaction');
var appUtil = require('../appUtil/appUtil');

module.exports = {
    basicOperations: new baseDao.basicOperations('transaction'),
    createTransaction: function (date, callback) {
        transactionColl.insert(query, baseDao._callback(function(users){
            callback(users);
        }));
    },
    updateTransaction: function (tid, joloid, st, resp, callback) {
        transactionColl.update({_id: baseDao.getId(tid)} , {$set : { tid : joloid, st : st, resp : resp}}, baseDao._callback(callback));
    }
}