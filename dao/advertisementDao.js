var model = require("../model/model");
var baseDao = require("./baseDao");
var advertisementColl = baseDao.getColl('advertisement');
var appUtil = require('../appUtil/appUtil');

module.exports = {
    basicOperations: new baseDao.basicOperations('advertisement'),
    getAdvertisement: function (uid, callback) {
        var options = {}, query = {};
        query["uids"] = {$ne : uid};
        advertisementColl.find(query).toArray(baseDao._callback(function(adds){
            callback(adds);
        }));
    },
    addThumbnail : function (_id, url, size, awsId, callback) {
        var obj = {};
        obj["$set"] = {};
        obj["$set"]["AdIcon"] = url;
        obj["$push"] = { awsLst : awsId };
        advertisementColl.update({ _id : _id }, obj, baseDao._callback(callback));
    },
    addRewards : function (uid, aid, callback) {
        var obj = {}, query = {};
        query["uids"] = {$ne : uid};
        query["_id"] = baseDao.getId(aid);
        query["LeftImpression"] = {$gt : 0};
        obj["$inc"] = {};
        obj["$inc"]["LeftImpression"] = -1;
        obj["$push"] = { uids : uid };
        advertisementColl.findAndModify(query, {}, obj, {new: true, fields:{AdGivingPrice : true}}, baseDao._callback(function(res){
            callback(res);
        }));
    }
}