// FIXME : Only required fields shall be returned by dao
var model = require("../model/model");
var baseDao = require("./baseDao");
var userColl = baseDao.getColl('user');
var fieldsToShow = {dvcs : false, updatedAt : false };
var appUtil = require('../appUtil/appUtil');
var transColl = baseDao.getColl('transaction');
userColl.ensureIndex( { "eml": 1 }, { unique: true, sparse: true }, function(err, res){
    console.log("ensuring unique sparse index for eml in user coll.","err",err,"res",res);
});

module.exports = {
    basicOperations: new baseDao.basicOperations('user'),
    CreateUpdateUser: function (fid, user, device, callback) {
        userColl.update({ dvcs: {$eq : device} }, {$pull : { dvcs : device}}, baseDao._callback(function(res){
            userColl.findAndModify(
                {eml: user.eml},
                {},
                {
                    $setOnInsert: user,
                    $addToSet : { dvcs : device},
                    $set: {fid: fid}
                },
                {upsert: true, new: true, fields: fieldsToShow},
                baseDao._callback(callback));
        }));
    },
    getUserById: function (_id, callback) {
        userColl.findOne({ _id: baseDao.getId(_id) }, fieldsToShow, baseDao._callback(function (res) {
            if (callback) callback(res);
        }));
    },
    getUserByEmail: function (eml, callback) {
        userColl.findOne({ eml: eml }, fieldsToShow, baseDao._callback(function (res) {
            if (callback) callback(res);
        }));
    },

    getUserPointLevelQuota: function (_id, callback) {
        userColl.findOne({ _id: baseDao.getId(_id) }, {pts: true, lvl:true, cmntQuota: true, pstQuota: true}, baseDao._callback(function (res) {
            if (callback) callback(res);
        }));
    },
    updateUserPtsQuotaLevelById: function (id, userFieldsToUpdate, posterId, callback) {
        var obj = {};
        var fields = {};

        if (userFieldsToUpdate.pts) {
            fields.pts = userFieldsToUpdate.pts;
        }else{
            fields.pts = 0;
        }
        if (userFieldsToUpdate.pcnt) {
            fields.pcnt = userFieldsToUpdate.pcnt;
        }
        obj.$inc = fields;
        userColl.findAndModify({ _id: baseDao.getId(id) }, {}, obj, {new : true}, baseDao._callback(function(res){
            if(res){
                if(posterId){
                    updatePostOwnerCount(posterId, userFieldsToUpdate, callback);
                }
                callback(res);
            }else{
                callback(null);
            }
        }));
    },
    
    updateInfo: function (user, callback) {
        var obj = {};
        var fields = {};

        if (user.lvl) {
            fields.lvl = user.lvl;
        }
        if (user.pstQuota) {
            fields.pstQuota = user.pstQuota;
        }
        if (user.cmntQuota) {
            fields.cmntQuota = user.cmntQuota;
        }

        if (fields.length == 0) {
            throw appUtil.exceptions.NONE_PARAMETERS();
        }
        obj.$set = fields;
        userColl.update({ _id: user._id }, obj, baseDao._callback(callback));
    },

    updateUserDetailById : function (_id, name, imgPath, img, tag, callback){
        var obj = {};
        var fields = {};
        if (imgPath) {
            //fields.dp = imgPath+img.name;
            fields.dp = imgPath;
        }
        if (name) {
            fields.name = name;
        }
        if (tag || (tag === "")) {
            fields.tag = tag;
        }
        obj.$set = fields;
        userColl.findAndModify({ _id: baseDao.getId(_id) }, {}, obj, {new: true, fields: fieldsToShow}, baseDao._callback(callback));
    },

    getUserInfoByIds: function (ids, callback) {
        var ar = [];
        for(var i=0; i<ids.length; i++){
            ar.push(baseDao.getId(ids[i]));
        }
        userColl.find({_id : { $in: ar}},{ name : true, dp  :true}).toArray(baseDao._callback(callback));
    },

    updateUserDevice : function (uid, device, callback) {
        userColl.update({ _id: baseDao.getId(uid) }, {$addToSet : { dvcs : device}}, baseDao._callback(callback));
    },

    getUserDevices: function (ids, callback) {
        var ar = [];
        for(var i=0; i<ids.length; i++){
            ar.push(baseDao.getId(ids[i]));
        }
        userColl.find({_id : { $in: ar}},{ name : true, dvcs : true}).toArray(baseDao._callback(callback));
    },
    resetUserQuota : function(lvl, pstQuota, cmtQuota, callback){
        userColl.update({ lvl: lvl }, {$set: { pstQuota : pstQuota, cmntQuota : cmtQuota }}, {multi : true}, baseDao._callback(function(res){
            var usersData = {};
            usersData["level"+lvl] = res;
            callback(usersData);
        }));
    },
    removeUserDevice : function (user, sessionId, callback) {
        userColl.update({_id: baseDao.getId(user._id)}, {$pull: {dvcs: {sessionId: sessionId}}}, {multi: true}, baseDao._callback(callback));
    },

    getMaximumCount : function(callback){
        userColl.aggregate(
            {
                $group:
                {
                    _id: null,
                    mpcnt: { $max: "$pcnt" },
                    mccnt: { $max: "$ccnt" },
                    mucnt: { $max: "$ucnt" },
                    mdcnt: { $max: "$dcnt" }
                }
            },
            baseDao._callback(callback)
        );
    },
    updatePostOwnerCount: updatePostOwnerCount,

    updateUserActiveStatus : function(uid, isActive, callback){
        userColl.update({ _id: baseDao.getId(uid) }, {$set : { isActive : isActive}}, baseDao._callback(callback));
    },

    getActiveUserDevices: function (uid, callback) {
        userColl.find({_id: {$ne: baseDao.getId(uid)}, isActive : true},{ dvcs : true}).toArray(baseDao._callback(callback));
    },

    getUsersWithRanks: function (uid, callback) {
        var options = {};
        options.sort = [];
        options.sort.push(["pts","desc"]);
        userColl.find({}, {name:true, dp:true, pts : true}, options).skip(0).limit(100).toArray(baseDao._callback(function(users){
            var currentRank, points;
            for(var i in users){
                var rank = parseInt(i)+1;
                users[i].rank = rank;
                if(users[i]._id == uid){
                    currentRank = rank;
                    points = users[i].pts;
                }
            }
            callback(users, currentRank, points);
        }));
    },

    getTopUsers: function (callback) {
        var options = {};
        options.sort = [];
        options.sort.push(["pts","desc"]);
        userColl.find({}, {name:true, dp:true, pts : true}, options).skip(0).limit(3).toArray(baseDao._callback(function(users){
            for(var i in users){
                var rank = parseInt(i)+1;
                users[i].rank = rank;
            }
            callback(users);
        }));
    },

    resetUserPoints : function(callback){
        userColl.update({} , {$set : { pts : 0, updatedAt : appUtil.getCurrentUTCTime()}}, {multi: true}, baseDao._callback(callback));
    },

    addUserPoints : function(uid, points, event, callback){
        var ts =  appUtil.getCurrentUTCTime();
        userColl.findAndModify({_id: baseDao.getId(uid)}, {}, {$inc : { pts : points, updatedAt : ts}}, {new : true, fields:{_id:false, pts:true}}, baseDao._callback(function(res){
            var eventsColl = baseDao.getColl('events');
            var event = new model.db.Events(event, uid, ts);
            eventsColl.insert(event, baseDao._callback(function(result){
                callback(res);
            }));
        }));
    },

    getUserPoints: function (_id, callback) {
        userColl.findOne({ _id: baseDao.getId(_id) }, {_id: false, pts : true}, baseDao._callback(function (res) {
            if (callback) callback(res);
        }));
    },

    addAmount : function(uid, amt, callback){
        userColl.update({_id: uid} , {$inc : { amt : amt, total : amt}, $set:{amtUat : appUtil.getCurrentUTCTime()}}, baseDao._callback(callback));
    },

    addRecharge : function(uid, mobile, amount, operator, callback){
        var ts = appUtil.getCurrentUTCTime();
        userColl.update({_id: baseDao.getId(uid), amt: {$gte: amount}}, {$inc : { amt : -amount}, $set:{amtUat : appUtil.getCurrentUTCTime()}}, baseDao._callback(function(res){
            if(res) {
                var transaction = new model.db.Transactions(operator, amount, mobile, uid, ts);
                transColl.insert(transaction, baseDao._callback(function (result) {
                    callback(result);
                }));
            }else{
                throw appUtil.exceptions.INSUFFICIANT_BALANCE();
            }
        }));
    }
}

function updatePostOwnerCount(posterId, userFieldsToUpdate, callback){
    var posterObj = {};
    var posterFields = {};
    if (userFieldsToUpdate.ccnt) {
        posterFields.ccnt = userFieldsToUpdate.ccnt;
    }
    if (userFieldsToUpdate.lcnt) {
        posterFields.lcnt = userFieldsToUpdate.lcnt;
    }
    if (userFieldsToUpdate.pcnt) {
        posterFields.pcnt = userFieldsToUpdate.pcnt;
    }
    posterObj.$inc = posterFields;
    userColl.update({ _id: baseDao.getId(posterId) }, posterObj, baseDao._callback(null));
}
