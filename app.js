var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var dbName = "ClickdCash";
var compression = require('compression');
var mongodb = require('./dao/daoConfig');
var multer  = require('multer');
var uploadingDir = require('./appUtil/appUtil').getUserHome()+"/ClickdCash/uploads/";
var app = express();
// Initialize connection once
mongodb.init(dbName, function(database){
    // view engine setup
    app.set('views', path.join(__dirname, 'views'));
    app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
    app.use(logger('dev'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(cookieParser());
    app.use(express.static(path.join(__dirname, 'public')));
    app.use(compression());
    app.use(multer({ dest: uploadingDir}));
    if(database) {
        var cookieSecret = 'someRandomSecretHereToAvoidCookieTempering';
        app.use(session({
            secret: cookieSecret,
            resave: true,
            saveUninitialized: true,
            store: new MongoStore({
                db: dbName
            })
        }));
        require("./controller");
        var docs_handler = express.static(__dirname + '/node_modules/swagger-ui/dist');
        app.get(/^\/docs(\/.*)?$/, function (req, res, next) {
            if (req.url === '/docs') { // express static barfs on root url w/o trailing slash
                res.writeHead(302, {'Location': req.url + '/'});
                res.end();
                return;
            }
            // take off leading /docs so that connect locates file correctly
            req.url = req.url.substr('/docs'.length);
            return docs_handler(req, res, next);
        });
    }
});

module.exports = app;
