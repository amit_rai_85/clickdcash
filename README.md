Following are the steps for Voxcast backend to run

Prerequisites :-
NodeJs is installed on the system
Credentials free Mongo server has been installed on the system which is listening on 27017 (default port) port.
Project Setup :-
Our app requires number of third party npm librarys. Here are the steps to install them
npm install express # webappframefork
npm install mongodb # mongodb client
npm install consolidate # templating engine
npm install compression #  Compression middleware for compressing response
npm install express-session # Session management
npm install body-parser # parsing request body
npm install twitter # interact with twitter servers
npm install loggly # loggly client
npm install connect-mongo # persisting sessions into db
npm install s3 # connecting with amazon servers to upload and delete data
npm install gm # Image compresion and resizing
npm install multer # facilitate multipart uploads to server
npm install -g --save collections
npm install node-ffprobe #facilitate in computation of video length
Note : gm requires graphicsmagick or imagemagick to be installed on machine.

Same can be installed in ubuntu via
   apt-get install graphicsmagick
   apt-get install imagemagick

For mac
   brew install imagemagick
   brew install graphicsmagick

For running the app simply hit node app.js

import into eclipse:
nodeclipse -p

using DocBlockr as a sublime extention for jsDocs
Different environments in system.json - "local", "uat", "prod";

--steps to generate access key for using its api
--Visit the APIs console at https://code.google.com/apis/console and log in with your Google Account
--and click the API Access option
--google reverse geocoding api key for dev server: AIzaSyAnLfiq1dyKq6Tbgx7th5xmRkm_MfpkufQ
--google reverse geocoding api key for prod server: AIzaSyBgn0iDUX8ufP9S2WwsvEoYDEO727F8_oM
--under property/system.json replace the value of below key with above ones
--"grgApi" : "AIzaSyCCwBdShCnMBWYaFfugZJV0XrqWr-BEKDM" //it needs to be updated


#Changes in Files when move to 'Production'
appUtil/appConstants : change adminEmail and adminName
property/system.json : change key's value
#Prod System.json
{
    "appBaseUrl":"http://54.148.96.117:3001",
    "serverName":"Vox-prod",
    "env":"prod",
    "awsBucket" : "voxprod",
    "grgApi" : "AIzaSyBgn0iDUX8ufP9S2WwsvEoYDEO727F8_oM"
}
#Server setup
#Install Mongo
1. sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
2. echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' | sudo tee /etc/apt/sources.list.d/mongodb.list
3. sudo apt-get update
4. sudo apt-get install mongodb-org

#Credentials free Mongo server has been installed on the system which is listening on 27017 (default port) port.

#Install node
1. sudo apt-get install python
2. sudo apt-get install make
3. sudo apt-get install g++
4. wget http://nodejs.org/dist/v0.10.32/node-v0.10.32.tar.gz
5. tar -xvf node-v0.10.32.tar.gz
6. cd node-v0.10.32/
7. ./configure
8. make
9. sudo make install

#Starting up the server
* npm install -g forever #Only one time
* forever start -l ./voxLog -a app.js
* vi /root/.forever/voxLog

#Stoping the server
* forever stop app.js

#Upload directory of the application
* mkdir -p ~/vox/uploads

#ffmpeg is required for node-ffprobe package to work
#Steps to install ffmpeg on linux
1. wget http://ffmpeg.org/releases/ffmpeg-2.4.3.tar.bz2
2. tar -xvf ffmpeg-2.4.3.tar.bz2
3. cd ffmpeg-2.4.3/
4. ./configure --disable-yasm
5. make
6. sudo make install

#change 'url' in swaggerUi object /node_modules/swagger-ui/dist/index.html