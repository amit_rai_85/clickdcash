/**
 * Created by amit on 6/11/14.
 */
var dao = require("../dao");
var model = require("../model/model");
var appUtil = require('../appUtil/appUtil');

var savePost = function (user, pUrl, vUrl, msg, callback) {
    var time = appUtil.getCurrentUTCTime();
    var invu = [];
    var post = new model.db.Post(msg, user._id, pUrl, vUrl, time, invu);
    dao.postDao.basicOperations.insert(post, function(post){
        callback(post);
    });
};

function deleteFromAws(aws){
    var awsId = aws.split("/");//extract amazon key from url
    if(awsId.length>0)
        aws = awsId[awsId.length - 1];
    appUtil.fileStorage.delete(aws, function (err) {
        if (err) {
            info.errorMessage = "Unable to delete post file: " + aws;
            appUtil.logger.ERROR(info, "deletePost");
        }
        else {
            // post image successfully deleted from AWS S3
            appUtil.logger.INFO({
                deleted : aws
            }, "deletePost");
        }
    });
}


module.exports = {
    savePost : savePost,
    deletePost : function(pid, uid, callback){
        dao.postDao.getPostImages(pid, function(result){
            dao.postDao.deletePost(pid, uid, function(res){
                if(res && result.length>0){
                    if(result[0].pUrl){
                        result[0].pUrl.forEach(function (aws) {
                            deleteFromAws(aws);
                        });
                    }
                    if(result[0].vUrl){
                        result[0].vUrl.forEach(function (aws) {
                            if(aws.tn) {
                                deleteFromAws(aws.tn);
                            }
                            if(aws.vi){
                                deleteFromAws(aws.vi);
                            }
                        });
                    }
                    callback(res);
                }else{
                    throw appUtil.exceptions.POST_DELETED();
                }
            });
        });
    },
    getPost : function(uid, lat, lng, startIn, count, getRecent, pId, direction, isMyPost, user, callback){
        if(lat && lng) {
            var loc = new model.db.Loc(lng, lat);
            dao.locationDao.getNearestCity(loc.lt, loc.lg, function (city, area) {
                dao.postDao.getPost(uid, loc.lt, loc.lg, city, startIn, count, getRecent, pId, direction, isMyPost, user, function (posts) {
                    callback(posts);
                }, area);
            });
        }else{
            dao.postDao.getPost(uid, null, null, null, startIn, count, getRecent, pId, direction, isMyPost, user, function (posts) {
                callback(posts);
            }, null);
        }
    },
    addComment: dao.postDao.addComment,
    deleteComment : dao.postDao.deleteComment,
    findPostbyUid: dao.postDao.findPostbyUid,
    getPostDetail : dao.postDao.getPostDetail,
    votePost : dao.postDao.votePost,
    addReport : dao.postDao.addReport,
    IsInTrending : dao.postDao.IsInTrending,
    getComments : function(pid, callback){
        var result = [];
        var invArr = [];
        var mapUser = {};
        dao.postDao.getComments(pid, function(posts){
            if(posts && posts.pst && posts.pst.length >0) {
                result = posts.pst;
                for (var i = 0; i < result.length; i++) {
                    invArr = appUtil.unionArray(invArr, [result[i].uid]);
                }
                dao.userDao.getUserInfoByIds(invArr, function (ud) {
                    for (var count = 0; count < ud.length; count++) {
                        var usd = {};
                        usd.name = ud[count].name;
                        usd.img = ud[count].dp;
                        mapUser[ud[count]._id] = usd;
                    }
                    for (var i in result) {
                        result[i]["img"] = mapUser[result[i].uid].img;
                        result[i]["name"] = mapUser[result[i].uid].name;
                    }
                    callback(result);
                });
            }else{
                callback(result);
            }
        });
    }
}