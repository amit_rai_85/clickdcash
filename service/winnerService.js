var dao = require("../dao");
var model = require("../model");
var appUtil = require("../appUtil");

module.exports = {
    InsertIntoWinner : function(uid, date, points, name, imgPath, rank, callback) {
        var user = new model.db.Winners(uid, date, points, name, imgPath, rank);
        dao.winnerDao.basicOperations.insert(user, function(user){
            callback(user);
        });
    },
    getWinners : dao.winnerDao.getWinners
}