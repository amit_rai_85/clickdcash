var appUtil = require('../appUtil');
var notif = require('../property').getNotif();

var hostname = "sendgrid.com";
var port = 80;
var path = "/api/mail.send.json";

var API_USER = "api_user";
var API_KEY = "api_key";
var RECEIVER_EMAIL = "to";
var RECEIVER_NAME = "toname";
var SUBJECT = "subject";
var TEXT = "text";
var HTML = "html";
var SENDER_EMAIL = "from";
var SENDER_NAME = "fromname";


/**
 * sends email as per this email message. senderEmail and senderName are optional
 * @param  {[type]} emailMessage - json with following fields senderName, senderEmail, subject, body, receiverEmail, receiverName
 * @param {[type]} callback optional
 * @return {[type]} null;
 */
function sendMail(emailMessage, callback){
	var params = {};
	params[API_USER] = notif.sgUser;
	params[API_KEY] = notif.sgKey;
	if(emailMessage.senderName == null) {
		params[SENDER_NAME] = notif.sgFrom;
	} else {
		params[SENDER_NAME] = emailMessage.senderName;
	}
	if(emailMessage.senderEmail == null) {
		params[SENDER_EMAIL] = notif.sgFromName;
	} else {
		params[SENDER_EMAIL] = emailMessage.senderEmail;
	}
	params[SUBJECT] = emailMessage.subject;
	params[TEXT] = "";

    // Get footer content for email
    var emailBody = "";
    emailBody += "\n";//header
    emailBody += emailMessage.body;
    emailBody += ("<br/><br/>Thanks,<br/>The Voxcast Team");

	params[HTML] = emailBody.toString();
	params[RECEIVER_EMAIL] = emailMessage.receiverEmail;
	params[RECEIVER_NAME] = emailMessage.receiverName;
	appUtil.appHttpClient.post(hostname, port, path, params, function(res){
		if(callback){
			callback(res);
		}else{
			console.log("sendgrid response : "+ res);
		}
	});
}

exports.sendMail = sendMail;