var dao = require("../dao");
var model = require("../model");
var appUtil = require("../appUtil");
var sysProp = require('../property').getSystemProp();
var ptsProp = require('../property').getPointsProp();
var recharge = require("../recharge");

module.exports = {
    CreateUpdateFbUser : function(fbUser, device, callback) {
        var gn = model.db.GENDER.FEMALE;
        var imgPath = null;
        if (fbUser.gender === "male") {
            gn = model.db.GENDER.MALE;
        }
        if (fbUser.picture.data.url) {
            imgPath = fbUser.picture.data.url;
        }
        var user = new model.db.User(null, fbUser.email,fbUser.name, imgPath, gn, model.db.USER_STATE.ACTIVE, appUtil.getCurrentUTCTime,
            fbUser.referedBy);

		dao.userDao.CreateUpdateUser(fbUser.id, user, device, function(res, raw) {
            callback(res);
		});
	},
    updateUserDetailById : dao.userDao.updateUserDetailById,
    getUserById : dao.userDao.getUserById,
    getUserPointLevelQuota: dao.userDao.getUserPointLevelQuota,
    updateInfo : dao.userDao.updateInfo,
    updateUserPtsQuotaLevelById : function(uid, actionType, postDetails, callback){
        var userFieldsToUpdate = {};
        var posterId = null;
        if(postDetails){
            posterId = postDetails.uid;
        }
        if(actionType == model.db.NOTI_TYPE.NEW){
            userFieldsToUpdate.pts = ptsProp.addNewPost;
            userFieldsToUpdate.pcnt = 1;
        }
        else if(postDetails && actionType == 1){
            if (actionType === model.db.NOTI_TYPE.LIKE) {
                if(uid != posterId && postDetails.likeClicked && postDetails.likeClicked.indexOf(uid) > -1)
                    userFieldsToUpdate.pts = ptsProp.likePts;
                else
                    userFieldsToUpdate.pts = 0;
                userFieldsToUpdate.lcnt = postDetails.lcnt;
            }
        }
        else if(postDetails && actionType == model.db.NOTI_TYPE.COMMENT){
            userFieldsToUpdate.ccnt = 1;
            if(uid == posterId){
                userFieldsToUpdate.pts = 0;
            }else{
                userFieldsToUpdate.pts = ptsProp.commentOnOtherPost;
            }
        }
        dao.userDao.updateUserPtsQuotaLevelById(uid, userFieldsToUpdate, posterId, function(res){
            callback(res);
        });
    } ,
    getUserInfoByIds : dao.userDao.getUserInfoByIds,
    resetUserQuota : resetUserQuota,
    updateUserDevice : dao.userDao.updateUserDevice,
    getUserDevices : dao.userDao.getUserDevices,
    getPostsAndMappedUsers : function(uid, result, callback){
        var invArr = [];
        var mapUser = {};
        for(var i=0; i<result.length; i++){
            invArr = appUtil.unionArray(invArr, [result[i].uid]);
        }
        dao.userDao.getUserInfoByIds(invArr, function(ud){
            for(var count = 0; count < ud.length; count++){
                var usd = {};
                usd.name = ud[count].name;
                usd.img = ud[count].dp;
                mapUser[ud[count]._id] = usd;
            }
            for(var i in result){
                result[i]["img"] = mapUser[result[i].uid].img;
                result[i]["name"] = mapUser[result[i].uid].name;
            }
            callback(result);
        });
    },
    removeUserDevice: dao.userDao.removeUserDevice,
    getMaximumCount : dao.userDao.getMaximumCount,
    getUserByEmail : dao.userDao.getUserByEmail,
    updatePostOwnerCount: dao.userDao.updatePostOwnerCount,
    updateUserActiveStatus : dao.userDao.updateUserActiveStatus,
    getUsersWithRanks : dao.userDao.getUsersWithRanks,
    getTopUsers : dao.userDao.getTopUsers,
    addUserPoints : dao.userDao.addUserPoints,
    getUserPoints : dao.userDao.getUserPoints,
    addRecharge : function(uid, mobile, amount, operator, callback){
        dao.userDao.addRecharge(uid, mobile, amount, operator, function(res){
            if(res){
                recharge.sendRechargeRequest(mobile, amount, operator, res[0]._id.toString(), function(resp){
                    var joloTranscationId = "", transactionId = "", status = "";
                    if(resp){
                        var arr = resp.split(",");
                        if(arr.length > 0 && arr[0].toLowerCase() != "failed" && arr[1].toLowerCase() == "success"){
                            joloTranscationId = arr[0];
                            status = arr[1];
                            transactionId = arr[5];
                            dao.transactionDao.updateTransaction(transactionId, joloTranscationId, status, resp, function(res){
                                if(res){
                                    callback(1);
                                }else{
                                    callback(0);
                                }
                            });
                        }else{
                            throw appUtil.exceptions.RECHARGE_ERROR();
                        }
                    }
                    else{
                        throw appUtil.exceptions.RECHARGE_ERROR();
                    }
                });
            }
            else{
                throw appUtil.exceptions.RECHARGE_ERROR();
            }
        });
    }
};

function PerformUserQuotaResetJob(){
    dao.userDao.getTopUsers(function(users){
        dao.userDao.resetUserPoints(function(res){
            if(res && users && users.length > 0) {
                var date = appUtil.getLastDate();
                var i = users.length - 1;
                (function loop() {
                    if (i >= 0) {
                        dao.userDao.addAmount(users[i]._id, appUtil.appConstants.Amounts[i], function (res) {
                            if (res) {
                                var user = new model.db.Winners(users[i]._id, date, users[i].pts, users[i].name, users[i].dp, users[i].rank);
                                dao.winnerDao.basicOperations.insert(user, function (user) {
                                    if (i == 0) {
                                        dao.postDao.pushIntoArchived();
                                        resetUserQuota();
                                    } else {
                                        i--;
                                        loop();
                                    }
                                });
                            }else{
                                console.log("Amount update failed for user : " + users[i]._id.toString());
                            }
                        });
                    }
                }());
            }
            else{
                console.log("reset points failed or no users found!!");
            }
        });
    });
}

function resetUserQuota(){
    var today = new Date();
    var quotaResetDate = Date.UTC(today.getUTCFullYear(),today.getUTCMonth(),today.getUTCDate()+1);
    var timeLeftInQuotaReset = parseInt(quotaResetDate - appUtil.getCurrentUTCTime());
    console.log("Quota will be reset after seconds:" + timeLeftInQuotaReset/1000);
    setTimeout(function () {
        PerformUserQuotaResetJob();
    }, timeLeftInQuotaReset);
}