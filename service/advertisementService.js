var dao = require("../dao");
var model = require("../model");
var appUtil = require("../appUtil");

module.exports = {
    insertIntoAdvertisement : function(advertisement, callback) {
        var AdType = advertisement.AdType;
        var AdName = advertisement.AdName;
        var Package = advertisement.Package;
        var URL = advertisement.URL;
        var AdIcon = advertisement.AdIcon;
        var AdImage = advertisement.AdImage;
        var vendorName = advertisement.vendorName;
        var AdSubTitle = advertisement.AdSubTitle;
        var AdDownText = advertisement.AdDownText;
        var AdExtraText = advertisement.AdExtraText;
        var AdTotalPrice = advertisement.AdTotalPrice;
        var Active = 1;
        var TotalImpression = advertisement.TotalImpression;
        var LeftImpression = advertisement.TotalImpression;
        var vendorEmail = advertisement.vendorEmail;
        var email = advertisement.email;
        var IndirectReward = advertisement.IndirectReward;
        var DirectReward = advertisement.DirectReward;
        var startDate = advertisement.startDate;
        var endDate = advertisement.endDate;
        var ts = appUtil.getCurrentUTCTime();
        var ButtonText = advertisement.ButtonText;
        var ClickUrl = advertisement.ClickUrl;
        var advertisementObj = new model.db.Advertisement(AdType,AdName,Package,URL,AdIcon,AdImage,vendorName,AdSubTitle,AdDownText,
            AdExtraText,AdTotalPrice,Active,TotalImpression,LeftImpression,vendorEmail,email,
            IndirectReward, DirectReward, ts, startDate, endDate, ButtonText, ClickUrl);
        dao.advertisementDao.basicOperations.insert(advertisementObj, function(res){
            callback(res);
        });
    },
    getAdvertisement : dao.advertisementDao.getAdvertisement,
    addThumbnail : dao.advertisementDao.addThumbnail,
    addRewards : function(uid, aid, callback){
        dao.advertisementDao.addRewards(uid, aid, function(res){
            if(res){
                dao.userDao.addUserPoints(uid, res.AdGivingPrice, aid, function(res){
                    if(res)
                        callback(1);
                    else
                        callback(0);
                });
            }else{
                callback(0);
            }
        });
    }
}