var mailSender = require('./mailSender');

module.exports = {
	mailSender : mailSender,
	userService : require('./userService'),
    postService : require('./postService'),
    winnerService : require('./winnerService'),
    advertisementService : require('./advertisementService')
}