var model = require("../model");
var appUtil = require('../appUtil');
var social = require('../property').getSocial();
FB_PROFILE_FETCH_URL = "https://graph.facebook.com/me?fields=email,first_name,id,last_name,name,gender,picture.type(square).width(180).height(180)&access_token=";
GP_PROFILE_FETCH_URL = "https://www.googleapis.com/plus/v1/people/me?access_token=";
LN_PROFILE_FETCH_URL = "https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address,pictureUrl)?format=json&oauth2_access_token=";

module.exports = {
					/**
					 * [getFBProfile description]
					 * @param  {[type]}   token    [description]
					 * @param  {Function} callback(fbUser) - fbUser is jsonObject with fields
					 *                                     first_name, last_name, id, email, gender;
					 * @return {[type]}            [description]
					 */
	getFBProfile : function(token, callback){
		appUtil.appHttpClient.getSecure(FB_PROFILE_FETCH_URL+token, function(resp){
			var retVal = JSON.parse(resp);
			if(retVal.error){
                throw appUtil.exceptions.INCORRECT_FB_TOKEN();
			}
			if(!retVal.email){
                throw appUtil.exceptions.EMAIL_ACCESS_DENIED();
			}
			callback(retVal);
		});
	}, 

						/**
					 * [getFBProfile description]
					 * @param  {[type]}   token    [description]
					 * @param  {Function} callback(gpUser) - gpUser is jsonObject with fields
					 *                                     firstName, lastName, id, email, gender, image
					 * @return {[type]}            [description]
					 */
	getGPProfile : function(token, callback){
		appUtil.appHttpClient.getSecure(GP_PROFILE_FETCH_URL+token, function(resp){
			console.log(resp);
			var res = JSON.parse(resp);
			if(res.error){
                throw appUtil.exceptions.INCORRECT_GP_TOKEN();
			}else{
                if(!res.emails || res.emails.length<=0){
                    throw appUtil.exceptions.EMAIL_ACCESS_DENIED();
                }
				var gpUser = {};
				gpUser.gender = res.gender; 
				gpUser.email = res.emails[0].value;
				gpUser.id = res.id;
				gpUser.firstName = res.name.givenName;
				gpUser.lastName = res.name.familyName;
				gpUser.dp = res.image.url;
				callback(gpUser);
			}
		});
	},
	
	getTwProfile : function(accessTokenKey, accessTokenSecret, callback){
		var twit = new twitter({
		    consumer_key: social.twKey,
		    consumer_secret: social.twSecret,
		    access_token_key: accessTokenKey,
		    access_token_secret: accessTokenSecret
		});
		twit.verifyCredentials(function(res) {
			//debugger;
			if(res.type && res.type==='error'){
                throw appUtil.exceptions.INCORRECT_TW_TOKEN();
			}
			var twUser = {};
			twUser.gender = res.gender; 
			twUser.id = res.id_str;
			twUser.name = res.name;
			twUser.screen_name = res.screen_name;
            twUser.imgUrl = res.profile_image_url;
			callback(twUser);
	    });
	},

    getLiProfile : function(token, callback){
        appUtil.appHttpClient.getSecure(LN_PROFILE_FETCH_URL+token, function(resp){
            var res = JSON.parse(resp);
            if(res.errorCode === 0){
                throw appUtil.exceptions.INCORRECT_LI_TOKEN();
            }else{
                if(!res.emailAddress){
                    throw appUtil.exceptions.EMAIL_ACCESS_DENIED();
                }
                var lnUser = {};
                lnUser.gender = res.gender;
                lnUser.email = res.emailAddress;
                lnUser.id = res.id;
                lnUser.firstName = res.firstName;
                lnUser.lastName = res.lastName;
                lnUser.dp = res.pictureUrl;
                callback(lnUser);
            }
        });
    }
}