var model = require("../model");
var appUtil = require('../appUtil');
var notf = require('../property').getNotif();
var mode = appUtil.appConstants.IsProduction ? 1 : 0;
//Set mode = 1 for live &
//mode = 0 for test
var JOLO_FETCH_URL = "http://joloapi.com/api/recharge.php?mode="+mode;
//userid=yourusername&key=yourapikey&operator=operatorcode&service=mobilenumber&amount=amount&orderid=youruniqueorderid";

module.exports = {
    sendRechargeRequest : function(mobile, amount, operatorcode, orderid, callback){
        JOLO_FETCH_URL = JOLO_FETCH_URL + "&userid="+notf.joloUserId + "&key="+notf.joloApiKey+"&operator="+operatorcode;
        JOLO_FETCH_URL = JOLO_FETCH_URL + "&service="+mobile + "&amount="+amount+"&orderid="+orderid;
        appUtil.appHttpClient.get(JOLO_FETCH_URL, function(resp){
            console.log(resp);
            callback(resp);
        });
    }
}