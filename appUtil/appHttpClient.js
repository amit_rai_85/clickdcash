var querystring = require('querystring');
var http = require('http');
var https = require('https');
var logger = require("./logger");
var TAG = "appHttpClient";


var getResp = function (res, callback) {
    var fullResp = '';
    res.setEncoding('utf8');
    res.on('data', function (chunk) {
        fullResp += chunk.toString();
    }).on('end', function () {
        callback(fullResp);
    });
}

function postRequest(hostname, port, path, headers, body, isSecure, callback) {
    var options = {
        hostname: hostname,
        port: port,
        path: path,
        method: 'POST',
        headers: headers
    };
    var client = null;
    if (isSecure) {
        client = https;
    } else {
        client = http;
    }
    var req = client.request(options, function (res) {
        var fullResp = '';
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            fullResp += chunk.toString();
        }).on('end', function () {
            callback(fullResp);
        });
    });
    req.on('error', function (e) {
        logger.ERROR({request: options, body: body, error: e.message}, TAG)
    });
    req.write(body);
    req.end();
}

module.exports = {

    postWithHeader: function (hostname, port, path, headers, body, isSecure, callback) {
        postRequest(hostname, port, path, headers, body, isSecure, callback);
    },

    post: function (hostname, port, path, params, callback) {
        var body = querystring.stringify(params);
        var headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': Buffer.byteLength(body)
        };
        postRequest(hostname, port, path, headers, body, false, callback);
    },
    get: function (url, callback) {
        http.get(url, function (res) {
            getResp(res, callback);
        }).on('error', function (e) {
            console.log("Got error: " + e.message);
        }).end();
    },
    getSecure: function (url, callback) {
        https.get(url, function (res) {
            getResp(res, callback)
        }).on('error', function (e) {
            console.log("Got error: " + e.message);
        }).end();
    }
}
