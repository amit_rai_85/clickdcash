var crypto = require('crypto');
var model = require('../model');
var emailProp = require('../property').getEmailProp();
var ptsProp = require('../property').getPointsProp();
var sysProp = require('../property').getSystemProp();
var appConstants = require("../appUtil/appConstants");

var getMd5 = function(val){
    var md5 = crypto.createHash('md5');
    md5.update(val);
    return md5.digest('hex');
};

/**
 * returns current time in utc
 * @returns {number}
 */
var getCurrentUTCTime = function(){
    var date = new Date();
    //get time always return milliseconds from UTC
    return date.getTime();
};

module.exports = {
	getErrorResponse : function(err){
		return new model.APIResponse(0, err);
	},

	getSuccessResponse : function(res){
		return new model.APIResponse(1, res);
	},

    getSuccessResponse : function(res, isLevelCross){
        return new model.APIResponse(1, res, isLevelCross);
    },

	getUserHome : function(){
		  return process.env.HOME || process.env.HOMEPATH || process.env.USERPROFILE;
	},

    getValidationError : function(res){
        delete res.validation;
        return new model.APIResponse(2, res);
    },

    getUserRegistrationMail : function(user){
        var emailMessage = {};
        emailMessage.senderName = null;
        emailMessage.senderEmail = null;
        emailMessage.subject = "VoxCast - User Register";
        emailMessage.body = emailProp.userRegistration
            .replace("%name%",user.name)
            .replace("%email%",user.eml);
        emailMessage.receiverEmail = appConstants.adminEmail;
        emailMessage.receiverName = appConstants.adminName;
        return emailMessage;
    },

    getReportAbuseMail : function(post){
        var emailMessage = {};
        emailMessage.senderName = null;
        emailMessage.senderEmail = null;
        emailMessage.subject = "VoxCast-Abuse reported in Post";
        emailMessage.body = emailProp.reportAbuse
            .replace("%post%",post);
        emailMessage.receiverEmail = appConstants.adminEmail;
        emailMessage.receiverName = appConstants.adminName;
        return emailMessage;
    },

    getPollReportAbuseMail : function(poll){
        var emailMessage = {};
        emailMessage.senderName = null;
        emailMessage.senderEmail = null;
        emailMessage.subject = "VoxCast-Abuse reported in Poll";
        emailMessage.body = emailProp.reportPollAbuse
            .replace("%poll%",poll);
        emailMessage.receiverEmail = appConstants.adminEmail;
        emailMessage.receiverName = appConstants.adminName;
        return emailMessage;
    },

	getMd5 : getMd5,

    getCurrentUTCTime : getCurrentUTCTime,

    isProd : function(){
        return sysProp.env === "prod";
    },

    unionArray : function (a, b) {
        for (var i = 0; i < b.length; i++)
            if (a.indexOf(b[i]) === -1)
                a.push(b[i]);
        return a;
    },
    /**
     *
     * @param obj
     * @returns {boolean}
     */
    isJSONEmpty : function (obj) {
        return Object.keys(obj).length === 0;
    },

    getCurrentDay : function () {
        return parseInt(getCurrentUTCTime() / (1000 * 60 * 60 * 24));
    },

    getUpdatedUserLevel: function(points, userFieldsToUpdate){
        var isLevelCrossed=false;
        if(parseInt(points.lvl) >= 0 && userFieldsToUpdate) {
            var upLevel = parseInt(points.lvl) + 1;
            if (points.pts + userFieldsToUpdate.pts >= ptsProp["ptsRequiredForLvl" + upLevel]) {
                if (upLevel < ptsProp["MaxLevel"]) {
                    userFieldsToUpdate.pstQuota = ptsProp["lvl" + upLevel + "PostQuota"];
                    userFieldsToUpdate.cmntQuota = ptsProp["lvl" + upLevel + "CommentQuota"];
                } else {
                    userFieldsToUpdate.pstQuota = 0;
                    userFieldsToUpdate.cmntQuota = 0;
                }
                userFieldsToUpdate.lvl = 1;
                isLevelCrossed = true;
            }
        }
        return isLevelCrossed;
    },

    /**
     *
     * @param str
     * @returns {boolean}
     */
    parseBoolean : function(str) {
        if(str === undefined){
            return str;
        }
        switch (String(str).toLowerCase()) {
            case "true":
            case "1":
            case "yes":
            case "y":
                return true;
            case "false":
            case "0":
            case "no":
            case "n":
            default:
                return false;
        }
    },
    /**
     * Merges two json objects to one
     * @param obj1
     * @param obj2
     */
    mergeJson : function(obj1 , obj2){
        var result={};
        for(var key in obj1) result[key]=obj1[key];
        for(var key in obj2) result[key]=obj2[key];
        return result;
    },
    /**
     * @return if given variable is string or not
     * @param s
     * @returns {boolean}
     */
    isString : function(s) {
        return typeof(s) === 'string' || s instanceof String;
    },

    /**
     *
     * @param jsonString
     * @returns {*}
     */
    toJson : function(jsonString){
        return JSON.parse(jsonString);
    },

    getLastDate : function(){
        var date = new Date();
        var UTCDate = new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() -1);
        return UTCDate.getFullYear() + '/' + (UTCDate.getMonth() + 1) + '/' + UTCDate.getDate();
    },

    getLastdays : function(days){
        var date = new Date();
        var daysArr = [];
        for(var i=1; i<= days; i++){
            var UTCDate = new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() -i);
            daysArr.push(UTCDate.getFullYear() + "/" + (UTCDate.getMonth() + 1)  + "/" + (UTCDate.getDate() -i));
        }
        return daysArr;
    },
	exceptions : require('./exceptions'),
	appHttpClient : require('./appHttpClient'),
	sessionUtil : require('./sessionUtil'),
	logger : require('./logger'),
    fileStorage : require("./fileStorage"),
    imageResizer : require("./imageResizer"),
    appConstants : require("./appConstants"),
    swaggerUtil : require("./swaggerUtil")

}
