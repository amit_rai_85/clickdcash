var s3 = require('s3');
var model = require('../model');
var bucketName = require('../property').getSystemProp().awsBucket;
logger = require("./logger");
var fs = require("fs");

var client = s3.createClient({
    maxAsyncS3: 20,     // this is the default
    s3RetryCount: 3,    // this is the default
    s3RetryDelay: 1000, // this is the default
    multipartUploadThreshold: 20971520, // this is the default (20 MB)
    multipartUploadSize: 15728640, // this is the default (15 MB)
    s3Options: {
        accessKeyId: "AKIAJOAN4ME6TPM4PQDA",
        secretAccessKey: "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"
        // any other options are passed to new AWS.S3()
        // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/Config.html#constructor-property
    }
});

module.exports = {
    upload : function(source, id, callback){
        var params = {
            localFile: source,

            s3Params: {
                Bucket: bucketName,
                Key: id,
                ACL: "public-read"
                // other options supported by putObject, except Body and ContentLength.
                // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#putObject-property
            }
        };
        var uploader = client.uploadFile(params);
        uploader.on('error', function(err) {
            callback(err, null);
        });
//        uploader.on('progress', function() {
//            console.log("progress", uploader.progressMd5Amount,
//                uploader.progressAmount, uploader.progressTotal);
//        });
        uploader.on('end', function() {
            callback(null, "https://s3-us-west-2.amazonaws.com/"+bucketName+"/"+id);
        });
    },

    delete : function(key, callback){
        var obs = [];
        for(var i in key){
            obs.push(
             /* required */
                {
                    Key: key /* required */
                }
                /* more items */
             );
        }
        var s3Params = {
            Bucket : bucketName,
            Delete : {
                Objects: obs
            }
            // http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#deleteObjects-property
        };
        var deleter = client.deleteObjects(s3Params);
        deleter.on("end", function(){
            callback(null, true);
        }).on("error", function(err){
            logger.ERROR(err,"AWS");
            callback(err, false);
        });
    },
    deleteFile : function(path, callback){
        fs.unlink(path, function (err) {
            if(err)
                logger.ERROR(err, "error deleting file at : " + path);
            callback();
        });
    }
}