var gm = require('gm');

module.exports = {
    resize : function(source, dest,  size, callback){
        gm(source)
            .resize(size, size)
            .autoOrient()
            .write(dest, function (err) {
                if(err){
                   if(callback) callback(err);
                }else{
                    if(callback) callback(null);
                };
            });
    }
};