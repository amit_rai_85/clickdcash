var exceptions = require('./exceptions');
function isUserLoggedIn(session){
    if(! session){
        return false;
    }
	return session.user !=null ;
};

module.exports = {
		loginUserIntoSession : function(session, user){
			session.user = user;
		},
		getLoggedInUserDetail : function(session, throwError){
			_throwError(throwError, session);
			if(isUserLoggedIn(session)){
				return session.user;
			}else{
				return null;
			}
		}, 
		isUserLoggedIn : isUserLoggedIn,
		getUserId : function(session, throwError){
			_throwError(throwError, session);
			if(isUserLoggedIn(session)){
				return session.user._id;
			}else{
				return null;
			}
		},
		clearSession : function(session){
            session.session.destroy();
		}
}

function _throwError(throwError, session){
	if(throwError && ! isUserLoggedIn(session)){
        throw exceptions.SESSION_DOESNOT_EXIST();
	}
}