module.exports = {
    allowedVideoSize : 20 * 1024 * 1024,
    allowedPictureSize : 5 * 1024 * 1024,
    allowedVideoLength : 5 * 60,
    allowedMessageLength : 200,
    PLATFORM : {
        IOS : 1,
        ANDROID : 2
    },
    fileServingPrefix : "/images/",
    anonymousImageUrl : "/static/images/anon.png",
    adminEmail : "amit.rai.85@outlook.com",
    adminName : "Amit Rai",
    TrendingScreenStartDate : 14*24*3600*1000,
    NotificationInterval : 1*15*60*1000,
    IsProduction : false,
    APP_BASE_URL: "http://localhost:3000",
    Amounts : [500, 300, 100]
}