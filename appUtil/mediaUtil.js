/**
 * Created by anil on 9/1/15.
 */
var exec = require("child_process").exec;
var model = require("../model");
var exceptions = require("./exceptionGenerator");
var gm = require('gm');


module.exports = {

    createThumbNailOfVideo : function(video , rotate , callback){
        if(parseInt(rotate)){
            rotate = " -vf \"transpose="+parseInt(rotate)/90+"\"";
        }else{
            rotate = "";
        }
        exec("ffmpeg -i " + video.path  + rotate +" -ss 00:00 -r 1 -an -vframes 1 -f mjpeg " + video.path  + ".jpeg",callback);
    },
    /**
     * This will take the pics Array and delete each and every pic from AWS
     * @param mediaArray : pics Array
     */
    deleteAllMediaFromAWS : function( mediaArray ){
        for(var index in mediaArray){
            module.exports.deleteMediaFromAWS( mediaArray[index].url);
            module.exports.deleteMediaFromAWS( mediaArray[index].tn);
            module.exports.deleteMediaFromAWS( mediaArray[index]["60"]);
            module.exports.deleteMediaFromAWS( mediaArray[index]["120"]);
            module.exports.deleteMediaFromAWS( mediaArray[index]["240"]);
            module.exports.deleteMediaFromAWS( mediaArray[index]["360"]);
            module.exports.deleteMediaFromAWS( mediaArray[index]["480"]);
            module.exports.deleteMediaFromAWS( mediaArray[index]["720"]);
        }
    },
    resetSDMediaFromAWS : function( mediaArray){
        module.exports.uploadSDMedia( mediaArray.url , "url");
        module.exports.uploadSDMedia( mediaArray.tn , "tn");
        module.exports.uploadSDMedia( mediaArray["60"],60) ;
        module.exports.uploadSDMedia( mediaArray["120"],120);
        module.exports.uploadSDMedia( mediaArray["240"],240);
        module.exports.uploadSDMedia( mediaArray["360"],360);
        module.exports.uploadSDMedia( mediaArray["480"],480);
        module.exports.uploadSDMedia( mediaArray["720"],720);
    },
    uploadSDMedia : function( url , size){
        if(!url){
            return;
        }
        var appUtil = require("../appUtil/appUtil");
        var awsKey = appUtil.fetchAWSKeyFromUrl(url);
        var image = { name : awsKey , path : appUtil.appConstants.MediaConstants.SD_PATH.replace("[size]",size)}
        module.exports.uploadImage(image ,"jpg" , null , true);
    },
    /**
     *
     * @param url
     * @param callback
     */
    deleteMediaFromAWS :function(url , callback ){
        if(!url){
            return;
        }
        var appUtil = require("../appUtil/appUtil");
        var awsKey = appUtil.fetchAWSKeyFromUrl(url);
        appUtil.fileStorage.delete(awsKey , function(response , result){
           if(!result){
               appUtil.logger.SEVERE( response , awsKey );
           }

            if(callback){
                callback(result);
            }
        });
    },
    /**
     *
     * @param media : image/video
     * @param type
     * @param extension
     * @param callback
     * @param isChatMedia : is given media is uploaded during chat
     */
    uploadMedia: function(media , type ,extension, rotate , callback , isChatMedia, cropDimensions, isFlipRequired){
        var appUtil = require("../appUtil/appUtil");
        extension = extension.toLowerCase();
        if(type == "image"){
            module.exports.uploadImage( media , extension , function(imageData){
                if(cropDimensions && !appUtil.isJSONEmpty(cropDimensions)){
                    module.exports.cropAndUpload( media , null , null , 200 , function(thumbData){
                        var image = new model.db.Media( thumbData.url , type , appUtil.getMediaKey() ,null,imageData.url );
                        callback(image);
                    }, cropDimensions);
                }else{
                    module.exports.autoResizeAndUpload( media , null , null , 200 , function(thumbData){
                        var image = new model.db.Media( imageData.url , type , appUtil.getMediaKey() ,thumbData.url );
                        if(isChatMedia){
                            module.exports.autoResizeAndUploadAll(media , null ,image ,image.key , function(imgData){
                                callback(imgData);
                            },isChatMedia ,rotate);
                        }else{
                            callback(image);
                        }
                    } , isChatMedia);
                }
            } , isChatMedia );
        }else if(type == "video"){
            module.exports.createThumbnailAndUploadVideo( media , extension , rotate ,  callback , isChatMedia, isFlipRequired);
        }else if(type == "audio"){
            module.exports.uploadAudio( media , extension , callback , isChatMedia);
        }
    },
    /**
     *
     * @param image
     * @param extension
     * @param callback
     * @param isChatMedia : is given media is uploaded during chat
     */
    uploadImage : function( image ,extension, callback ,isChatMedia){
        var appUtil = require("../appUtil/appUtil");
        image.name = appUtil.getNextSequence().toString() + "." + extension;
        appUtil.fileStorage.upload(image.path, image.name, function(err, url){
            if(err){
                throw exceptions.getErrorUploadingMediaException(err);
            } else if(callback) {
                var imageData = new model.db.Media( url , "image" , appUtil.getMediaKey());
                callback(imageData);
            }
        } , isChatMedia);
    },
    /**
     *
     * @param audio
     * @param extension
     * @param callback
     * @param isChatMedia : is given media is uploaded during chat
     */
    uploadAudio : function( audio ,extension, callback , isChatMedia ){
        var appUtil = require("../appUtil/appUtil");
        audio.name = appUtil.getNextSequence().toString() + "." + extension;
        appUtil.fileStorage.upload(audio.path, audio.name, function(err, url){
            if(err){
                throw exceptions.getErrorUploadingMediaException(err);
            } else if(callback) {
                var audioData = new model.db.Media( url , "audio" , appUtil.getMediaKey());
                callback(audioData);
            }
        } , isChatMedia);
    },
    /**
     *
     * @param video
     * @param extension
     * @param callback
     * @param isChatMedia : is given media is uploaded during chat
     */
    createThumbnailAndUploadVideo : function ( video , extension, rotate ,callback , isChatMedia, isFlipRequired ){
        var appUtil = require("../appUtil/appUtil");
        video.name = appUtil.getNextSequence().toString() + extension;
        var thumbFileName = video.name+".jpeg";

        appUtil.fileStorage.upload(video.path, video.name, function(err, url){
            if(err){
                throw exceptions.getErrorUploadingMediaException(err);
            } else {
                module.exports.createThumbNailOfVideo(video, rotate , function(err){
                    if(err)
                        throw exceptions.ErrorCreatingThumbException(err);
                    if(parseInt(isFlipRequired)) {
                        var flippedThumbFileName = video.name+"_flipped.jpeg";
                        module.exports.flip(video.path + ".jpeg", video.path + "_flipped.jpeg" , function(err) {
                            if (err)
                                throw exceptions.ErrorCreatingThumbException(err);
                            appUtil.fileStorage.upload(video.path + "_flipped.jpeg", flippedThumbFileName, function (err, tUrl) {
                                if (err) {
                                    throw exceptions.getErrorUploadingMediaException(err);
                                } else {
                                    var video = new model.db.Media(url, "video", appUtil.getMediaKey(), tUrl);
                                    callback(video);
                                }
                            }, isChatMedia);
                        });
                    }else{
                        appUtil.fileStorage.upload(video.path + ".jpeg", thumbFileName, function (err, tUrl) {
                            if (err) {
                                throw exceptions.getErrorUploadingMediaException(err);
                            } else {
                                var video = new model.db.Media(url, "video", appUtil.getMediaKey(), tUrl);
                                callback(video);
                            }
                        }, isChatMedia);
                    }
                });
            }
        } , isChatMedia);
    },

    /**
     *
     * @param multipart
     * @param callback { ext, size}
     */
    getImageData : function(multipart, callback){
        if(!multipart){
            throw exceptions.getMediaNotFoundException();
        }
        callback(
            {
                ext : multipart.extension.replace(".","").toUpperCase(),
                size : multipart.size
            }
        );
       /* probe(multipart.path, function (err, probeData) {
            if(err){
                throw exceptions.getImageParseException(err);
            }
            callback(
                {
                    ext : probeData.fileext.replace(".","").toUpperCase(),
                    size : probeData.format.size
                }
            );
        });*/
    },

    validateVideo : function( video){
        if(!video){
            throw exceptions.getMediaNotFoundException();
        }
        console.log("Video Details")
        console.log(video)
        var appUtil = require("../appUtil/appUtil");
        if(appUtil.appConstants.MediaConstants.ALLOWED_VIDEO_FORMAT.indexOf(video.fileext.replace(".","").toUpperCase()) == -1 ){
            throw exceptions.getUnsupportedFileException(video.fileext);
        }
        if (video.format.size > appUtil.appConstants.MediaConstants.ALLOWED_VIDEO_SIZE) {
            throw exceptions.getInvalidVideoSizeException();
        }
        if (parseInt(video.format.duration) > appUtil.appConstants.MediaConstants.ALLOWED_VIDEO_DURATION) {
            throw exceptions.getInvalidDurationException();
        }
    },
    validateImage : function( image , extension , callback){
        var appUtil = require("../appUtil/appUtil");
        if(appUtil.appConstants.MediaConstants.ALLOWED_IMAGE_FORMAT.indexOf(extension) == -1 ){
            throw exceptions.getUnsupportedFileException(extension);
        }
        if (image.size > appUtil.appConstants.MediaConstants.ALLOWED_PIC_SIZE) {
            throw exceptions.getInvalidImageSizeException();
        }
        callback(extension , "image");
    },
    autoResizeAndUploadAll : function(image , userId , mediaData , mediaKey , callback , isChatMedia , rotate){
        module.exports.autoResizeAndUpload(image , userId , mediaKey , 60 , function(media , size){
            mediaData[size] = media.url;
                module.exports.autoResizeAndUpload(image , userId , mediaKey , 120 , function(media , size){
                    mediaData[size] = media.url;
                    module.exports.autoResizeAndUpload(image , userId , mediaKey , 240 , function(media , size){
                        mediaData[size] = media.url;
                        module.exports.autoResizeAndUpload(image , userId , mediaKey , 360 , function(media , size){
                            mediaData[size] = media.url;
                            module.exports.autoResizeAndUpload(image , userId , mediaKey , 480 , function(media , size){
                                mediaData[size] = media.url;
                                module.exports.autoResizeAndUpload(image , userId , mediaKey , 720 , function(media , size){
                                    mediaData[size] = media.url;
                                    callback(mediaData);
                                }, isChatMedia , rotate)
                            }, isChatMedia , rotate)
                        }, isChatMedia , rotate)
                    }, isChatMedia , rotate)
                }, isChatMedia , rotate)
        }, isChatMedia , rotate )
    },
    /**
     * Resizes Image to 240 and 480 versions , uploads to amazon and make a db entry
     * Asynchornous
     * @param source
     * @param userId
     * @param mediaKey
     * @param isChatMedia
     * @param size
     * @param callback
     */
    autoResizeAndUpload : function(image , userId , mediaKey , size , callback , isChatMedia){
        var tn = image.path.toString().substring(0,image.path.lastIndexOf("/")+1) + image.name+"_[size].jpg";
        var callback1 = function(response , path,size){
            if(response == null){
                module.exports.uploadImage({ path : path } , "jpg" , function(imageData){
                    if(callback){
                        callback(imageData , size);
                    }else{
                        var service = require("../service/service")
                        var mediaDao = require("../dao/mediaDao")
                        service.userService.updateThumb( userId , mediaKey , imageData.url , size );
                        mediaDao.updateMedia( userId , mediaKey , imageData.url , size );

                    }
                },isChatMedia);
            }
        }
        if(size){
            module.exports.resize(image.path, tn.replace("[size]", size), size, callback1);
        }else {
            module.exports.resize(image.path, tn.replace("[size]", 60), 60, callback1);
            module.exports.resize(image.path, tn.replace("[size]", 120), 120, callback1);
            module.exports.resize(image.path, tn.replace("[size]", 240), 240, callback1);
            module.exports.resize(image.path, tn.replace("[size]", 360), 360, callback1);
            module.exports.resize(image.path, tn.replace("[size]", 480), 480, callback1);
            module.exports.resize(image.path, tn.replace("[size]", 720), 720, callback1);
        }
    },
    /**
     *
     * @param source : source path of image
     * @param dest : dest path
     * @param size : can be any size , output resolution will be size*size
     * @param callback
     */
    resize : function(source, dest,  size, callback){
        gm(source)
            .resize(size, size)
            .autoOrient()
            .write(dest, function (err) {
                if(err){
                    if(callback) callback(err);
                }else{
                    if(callback) callback(null , dest ,size);
                };
            });
    },

    crop  :function(source, dest,  width, height, x, y, callback, imageName){
        gm(source)
            .size(function (err, size) {
                if (!err && size) {
                    gm(source)
                        .crop(width*size.width, height*size.height, x*size.width, y*size.height)
                        .autoOrient()
                        .write(dest, function (err) {
                            if(err){
                                if(callback) callback(err);
                            }else{
                                if(callback) callback(null, dest ,width, imageName);
                            };
                        });
                }
            });
    },

    cropAndUpload : function(image , userId , mediaKey , size , callback , cropDimensions){
        var callback1 = function(response , path,size, name){
            if(response == null){
                module.exports.uploadImage({ path : path } , "jpg" , function(imageData){
                    if(callback){
                        //update original media path and name attributes
                        image.path = path;
                        image.name = name;
                        callback(imageData , size);
                    }else{
                        var service = require("../service/service");
                        service.userService.updateThumb( userId , mediaKey , imageData.url , size );
                    }
                });
            }
        }

        if(size){
            var imageName = image.name +"_cropped.jpg";
            var tn = image.path.toString().substring(0,image.path.lastIndexOf("/")+1) + imageName;
            module.exports.crop(image.path, tn, cropDimensions.width, cropDimensions.height, cropDimensions.x, cropDimensions.y, callback1, imageName);
        }
    },

    flip : function(source, dest, callback){
        gm(source)
            .flop()//Creates a mirror image (horizontally).
            //.flip()//Creates a mirror image (vertically).
            .autoOrient()
            .write(dest, function (err) {
                if(err){
                    if(callback) callback(err);
                }else{
                    if(callback) callback(null , dest);
                };
            });
    },

    /**
     *
     * @param source : source path of image
     * @param callback
     */
    autoOrient : function(media, callback){
        var orientedImage = media.path.toString().substring(0,media.path.lastIndexOf("/")+1) + media.name+"_[orient].jpg";
        require("gm")(media.path)
            .autoOrient()
            .write(orientedImage, function (err) {
                if(!err){
                    media.path = orientedImage;
                    media.name = media.name+"_[orient].jpg";
                    callback(null);
                }else{
                    callback(err);
                }
            });
    }
}