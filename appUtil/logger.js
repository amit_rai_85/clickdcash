var loggly = require('loggly');
var serverName = require('../property').getSystemProp().serverName;
var client = loggly.createClient({
	token : "ddb5ea64-653f-49cd-b7af-216e70d4d2f4",
	subdomain : "mobicules",
	tags : [ "vox" ],
	json : true
});

var INFO = function(obj, tag) {
    console.info("message ",obj, "tag", tag);
	log('INFO', obj, tag);
};

function log(level, message, tags) {
	if (message.constructor === String) {
		message = {
			msg : message
		};
	}
	message.serverName = serverName;
	message.level = level;
	if(tags){
		if (tags.constructor !== Array) {
			tags = [ tags ];
		}
		client.log(message, tags, function(err, result) {
		// Do something once you've logged
		});
	}else{
		client.log(message);
	}
};

var ERROR = function(obj, tag) {
    console.error("message ",obj, "tag", tag);
	log('ERROR', obj, tag);
};

var WARN = function(obj, tag) {
    console.warn("message ",obj, "tag", tag);
	log('WARN', obj, tag);
};

module.exports = {
	INFO : INFO,
	ERROR : ERROR,
	WARN : WARN,
    logException : function(err, tag){
        log("ERROR", err.stack, tag);
    },
	requestLogger : function(req, res, next) {
		var appUtil = require('./appUtil'); //	WHY WAS THIS NOT WORKING ABOVE
		var timeStart = new Date().getTime();
		res.on('finish', function() {
			var time = new Date().getTime() - timeStart;
			var request = {
				path : req.path,
				query : req.query,
				method : req.method,
				resTime : time
			//	param : req.param
			};
			if(appUtil.sessionUtil.isUserLoggedIn(req.session)){
				request.userId = appUtil.sessionUtil.getUserId(req.session);
			}
			INFO(request, "reqLog");
		});
		next();
	}
}