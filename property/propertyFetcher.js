fs = require('fs');
var notif = JSON.parse(fs.readFileSync(__dirname+'/notification.json'));
var social = JSON.parse(fs.readFileSync(__dirname+'/social.json'));
var system = JSON.parse(fs.readFileSync(__dirname+'/system.json'));
var emailProp = JSON.parse(fs.readFileSync(__dirname + '/email.json'));
var pointsProp = JSON.parse(fs.readFileSync(__dirname + '/points.json'));
module.exports = {
	getNotif : function(){
		return notif;
	},
	getSocial : function(){
		return social;
	},
	getSystemProp : function(){
		return system;
	},
	getEmailProp :function(){
		return emailProp;
    },
    getPointsProp : function () {
        return pointsProp;
    }
}