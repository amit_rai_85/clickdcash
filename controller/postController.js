var app = require('../app');
var service = require('../service');
var model = require("../model");
var appUtil = require("../appUtil/appUtil");
var postControllerHelper = require('./helper/postControllerHelper');
var ptsProp = require('../property').getPointsProp();


var createPost = {};
createPost.nickname="createPost";
createPost.path="/post/create";
createPost.method="POST";
createPost.desc="create a new post";
createPost.paramType="form";
createPost.params = [{ name : "pics" , type: "file" , desc: "picture for post" , required : true},
    { name : "msg" , type: "string" , desc: "post message" , required : false}];
app.post(createPost.path, function(req, res){
    var user = appUtil.sessionUtil.getLoggedInUserDetail(req.session, true);
    var pics = req.files.pics;
    var msg = req.body.msg;
    var ht = postControllerHelper.validateCreatePostRequest(msg, pics);
    postControllerHelper.createPicArray(pics, function(picsUrls){
        service.postService.savePost(user, picsUrls, null, msg, function (post) {
            service.userService.updateUserPtsQuotaLevelById(user._id, model.db.NOTI_TYPE.NEW, post, function (resp) {
                app.sendSuccess(res, {post: post, points : resp.pts});
            });
        });
    });
});

var addComment = {};
addComment.nickname="addComment";
addComment.path="/post/add/comment";
addComment.method="POST";
addComment.desc="add comment on a post";
addComment.paramType="form";
addComment.params = [ { name : "postId" , type : "string" , desc:"PostId", required: true},
    { name : "msg" , type : "string", desc : "comment text", required:true}];
app.post(addComment.path, function (req, res) {
    var pid = req.body.postId;
    var msg = req.body.msg;
    var ts = appUtil.getCurrentUTCTime();
    var user = appUtil.sessionUtil.getLoggedInUserDetail(req.session, true);
    var uid = user._id;
    postControllerHelper.validateAddComment(pid, msg);
    service.postService.addComment(pid, msg, uid, ts, function (resFields) {
        if (resFields) {
            service.userService.updateUserPtsQuotaLevelById(uid, model.db.NOTI_TYPE.COMMENT, resFields, function (resp) {
                resFields["points"] = resp.pts;
                app.sendSuccess(res, resFields);
            });
        } else {throw  appUtil.exceptions.INVALID_POST_OPERATION(); }
    });
});

var deleteComment = {};
deleteComment.nickname="deleteComment";
deleteComment.path="/post/delete/comment";
deleteComment.method="POST";
deleteComment.desc="delete comment from a post";
deleteComment.paramType="form";
deleteComment.params = [ { name : "postId" , type : "string" , desc:"PostId", required: true},
    { name : "commentId" , type : "string", desc : "commentId", required:true}];
app.post(deleteComment.path, function (req, res) {
    var pid = req.body.postId;
    var cid = req.body.commentId;
    var user = appUtil.sessionUtil.getLoggedInUserDetail(req.session, true);
    var uid = user._id;
    postControllerHelper.validateDeleteComment(pid, cid);
    service.postService.deleteComment(pid, uid, cid, function (result) {
        app.sendSuccess(res, {result: result});
    });
});
var deletePost = {};
deletePost.nickname="deletePost";
deletePost.path="/post/delete/:postId";
deletePost.method="GET";
deletePost.desc="delete Post";
deletePost.paramType="path";
deletePost.params = [ {name: "postId" , type:"string" , desc: "postId to delete"}];
app.get(deletePost.path, function(req, res){
    var user = appUtil.sessionUtil.getLoggedInUserDetail(req.session, true);
    var pid = req.param('postId');
    service.postService.deletePost(pid, user._id, function(result){
        app.sendSuccess(res, {result : result});
    });
});


var getPosts = {};
getPosts.nickname="getPosts";
getPosts.path="/get/posts";
getPosts.method="POST";
getPosts.desc="get posts";
getPosts.paramType="form";
getPosts.params = [{ name : "cnt" , type: "number" , desc: "number of posts" , required : false},
    { name : "startIn" , type: "number" , desc: "starting index in results" , required : false},
    { name : "msg" , type: "string" , desc: "post message" , required : false},
    { name : "uid" , type: "string" , desc: "userid" , required : false},
    { name : "pid" , type: "string" , desc: "post id" , required : false},
    { name : "getRecent" , type: "boolean" , desc: "get recent posts or not" , required : false},
    { name : "direction" , type: "string" , desc: "direction(up or down. it will be used with post id always)" , required : false}];
app.post(getPosts.path, function(req, res){
    var user = appUtil.sessionUtil.getLoggedInUserDetail(req.session, true);
    var cnt = parseInt(req.body.cnt);
    var startIn = parseInt(req.body.startIn);
    var uid = req.body.uid;
    var getRecent = (req.body.getRecent === "true");
    var pid = req.body.pid;
    var direction = req.body.direction;
    var isMyPost;
    if(uid)
        isMyPost = (user._id === uid);
    service.postService.getPost(uid, null, null, startIn, cnt, getRecent, pid, direction,isMyPost, user, function(result){
        service.userService.getPostsAndMappedUsers(user._id, result, function(cRes){
            app.sendSuccess(res, cRes);
        });
    });
});
var likePost = {};
likePost.nickname="likePost";
likePost.path="/post/like/:postId";
likePost.method="GET";
likePost.desc="like or unlike a post";
likePost.paramType="path";
likePost.params = [ {name: "postId" , type:"string" , desc: "postId"}];
app.get(likePost.path, function (req, res){
    var postId = req.param('postId');
    var user = appUtil.sessionUtil.getLoggedInUserDetail(req.session, true);
    var uid = user._id;
    var ts = appUtil.getCurrentUTCTime();
    service.postService.votePost(postId, uid, ts, function (comResult, isLiked) {
        service.userService.updateUserPtsQuotaLevelById(uid, model.db.NOTI_TYPE.LIKE, comResult, function (result) {
            app.sendSuccess(res, {result: isLiked, points : result.pts});
        });
    });
});

var reportPost = {};
reportPost.nickname="reportPost";
reportPost.path="/post/report/:postId/:reportType";
reportPost.method="GET";
reportPost.desc="report a post";
reportPost.paramType="path";
reportPost.params = [ {name: "postId" , type:"string" , desc: "postId"},
    {name: "reportType" , type:"string" , desc: "report type(1 for abuse, 2 for spam)"}];
app.get(reportPost.path, function(req, res){
    var postId = req.param('postId');
    var reportType = parseInt(req.param('reportType'));
    model.db.REPORT_TYPE.validate(reportType);
    var user = appUtil.sessionUtil.getLoggedInUserDetail(req.session, true);
    service.postService.addReport(user._id, reportType, postId, function(result){
        if(result){
            var em = appUtil.getReportAbuseMail(JSON.stringify(result));
            service.mailSender.sendMail(em);
            app.sendSuccess(res, {result: 1});
        }else
        {
            app.sendSuccess(res, {result: 0});
        }
    });
});
var postDetail = {};
postDetail.nickname="postDetail";
postDetail.path="/post/:postId/detail";
postDetail.method="GET";
postDetail.desc="post detail";
postDetail.paramType="path";
postDetail.params = [ {name: "postId" , type:"string" , desc: "postId"}];
app.get(postDetail.path, function(req, res){
    var postId = req.param('postId');
    var user = appUtil.sessionUtil.getLoggedInUserDetail(req.session, true);
    service.postService.getPostDetail(postId, function(result){
        service.userService.getPostsAndMappedUsers(user._id, result, function(cRes){
            app.sendSuccess(res, cRes);
        });
    });
});

var getComments = {};
getComments.nickname="getComments";
getComments.path="/post/get/comments";
getComments.method="POST";
getComments.desc="delete comment from a post";
getComments.paramType="form";
getComments.params = [ { name : "postId" , type : "string" , desc:"PostId", required: true}];
app.post(getComments.path, function (req, res) {
    var pid = req.body.postId;
    var user = appUtil.sessionUtil.getLoggedInUserDetail(req.session, true);
    service.postService.getComments(pid, function (result) {
        app.sendSuccess(res, {result: result});
    });
});

module.exports = {
    apis : [createPost, addComment, deleteComment, deletePost, likePost, reportPost, postDetail, getPosts, getComments]
};