/**
 * Created by amit on 12/11/14.
 */
var model = require("../../model");
var appUtil = require("../../appUtil/appUtil");
var fs = require('fs');
var probe = require('node-ffprobe');
var exec = require("child_process").exec;

function extractHashtags(msg){
    var ht = [];
    var tl = msg.trim().split(' ');
    for (var i = 0; i < tl.length; i++) {
        if (tl[i].indexOf('#') >= 0) {
            var tem = tl[i].trim();
            while(tem.length > 0 && tem.charAt(0) == '#'){
                tem = tem.replace('#', '');
            }
            var lastIn = tem.length;
            if ((tem.charCodeAt(0) >= 97 && tem.charCodeAt(0) <= 122) || (tem.charCodeAt(0) >= 65 && tem.charCodeAt(0) <= 90)) {
                for (var j = 0; j < tem.length; j++) {
                    var strChar = tem.charCodeAt(j);
                    if (!((strChar >= 97 && strChar <= 122) || (strChar >= 65 && strChar <= 90) || (strChar >= 48 && strChar <= 57))) {
                        lastIn = j;
                        break;
                    }
                }
                tem = tem.substring(0, lastIn);
                ht.push(tem);
            }else{
                ht.push(tem);
            }
        }
    }
    return ht;
}

function validatePsize(s){
    if(s > appUtil.appConstants.allowedPictureSize){
        throw appUtil.exceptions.PICTURE_SIZE_EXCEEDED();
    }
}

function validateVideo(probeData){
    if (probeData.format.size > appUtil.appConstants.allowedVideoSize) {
        throw appUtil.exceptions.VIDEO_SIZE_EXCEEDED();
    }
    if (probeData.format.duration > appUtil.appConstants.allowedVideoLength) {
        throw appUtil.exceptions.VIDEO_DURATION_EXCEEDED();
    }
}

function validatePictures(pics){
    if(pics){
        if(pics instanceof Array){                    
        }else{
            var p = [];
            p.push(pics);
            pics = p;
        }
        for(var i in pics){
            validatePsize(pics[i].size);
        }
    }else{
        throw appUtil.exceptions.IMAGE_REQUIRED();
    }
}


function validateCreatePostRequest(msg,pics){
    var ht = [];
    if(msg) {
        if(msg.length > appUtil.appConstants.allowedMessageLength){
            throw appUtil.exceptions.COMMENT_LENGTH_EXCEEDED();
        }
    }
    validatePictures(pics);
    return ht;
}

function validateAddComment(pid, msg) {
    if (!pid) {
        throw appUtil.exceptions.POSTID_REQUIRED();
    }
    if (!msg) {
        throw appUtil.exceptions.COMMENT_REQUIRED();
    }
   
    if (msg.length > appUtil.appConstants.allowedMessageLength) {
        throw appUtil.exceptions.COMMENT_LENGTH_EXCEEDED();
    }
}

function validateDeleteComment(pid, cid, position) {
    if (!pid) {
        throw appUtil.exceptions.POSTID_REQUIRED();
    }
    if (!cid) {
        throw appUtil.exceptions.COMMENTID_REQUIRED();
    }
}

function createPicArray(pics, callback){
    var files = null;
    if(pics){
        files = [];
        if(pics instanceof Array){                    
        }else{
            var p = [];
            p.push(pics);
            pics = p;
        }
        var uploadCount = 0;
        for(var i in pics){
            appUtil.fileStorage.upload(pics[i].path, pics[i].name, function(err, url){
                if(err){
                    throw err;
                } else {
                    files.push(url);
                    uploadCount = uploadCount + 1;
                }if(uploadCount==pics.length){
                    pics.forEach(function (pic) {
                        appUtil.fileStorage.deleteFile(pic.path, function () {
                        });
                    });
                    callback(files);
                }
            });
        }
    }else{
        callback(files);
    }
}

function createVideoArray(videos, callback){
    if(videos instanceof Array){
    }else{
        var vArr = [];
        vArr.push(videos);
        videos = vArr;
    }
    var videoUrls = [];
    var folderPath = appUtil.getUserHome()+"/vox/uploads/";
    var uploadCount = 0;
    var videoLocalPath = [];
    for(var i in videos){
        (function(n){
            probe(videos[n].path, function (err, probeData) {
                validateVideo(probeData);
                var thumbFileName = videos[n].name+".jpeg";
                //run ffmpeg command to generate thumbnail of video
                exec("ffmpeg -i " + videos[n].path  + " -s 320x240 -ss 00:00 -r 1 -an -vframes 1 -f mjpeg " + videos[n].path  + ".jpeg", function(err){
                    fs.exists(folderPath+thumbFileName, function(exists) {
                        if (exists) {
                            appUtil.fileStorage.upload(videos[n].path, videos[n].name, function(err, url){
                                if(err){
                                    throw err;
                                } else {
                                    appUtil.fileStorage.upload(folderPath+thumbFileName, thumbFileName, function(err, tUrl){
                                        if(err){
                                            throw err;
                                        } else {
                                            videoLocalPath.push(folderPath+thumbFileName);
                                            var videoData = {};
                                            videoData.tn = tUrl;//pass the thumbnail url of video image
                                            videoData.vi = url;
                                            videoUrls.push(videoData);
                                            uploadCount=uploadCount + 1;
                                            if(uploadCount == videos.length){
                                                //remove files from server after processing
                                                videos.forEach(function (video) {
                                                    appUtil.fileStorage.deleteFile(video.path, function () {
                                                    });
                                                });
                                                videoLocalPath.forEach(function (localPath) {
                                                    appUtil.fileStorage.deleteFile(localPath, function () {
                                                    });
                                                });
                                                callback(videoUrls);
                                            }
                                        }
                                    });
                                }
                            });
                        } else {
                            throw appUtil.exceptions.THUMBNAIL_GENERATION_ERROR();
                        }
                    });
                });
            });
        })(i);
    }
}

module.exports = {
    validateCreatePostRequest : validateCreatePostRequest,
    validateAddComment : validateAddComment,
    validateDeleteComment : validateDeleteComment,
    validateVideo : validateVideo,
    createPicArray : createPicArray,
    createVideoArray : createVideoArray
}
