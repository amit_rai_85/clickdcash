var app = require("../app");
var appUtil = require('../appUtil');
var domain = require('domain');
var service = require('../service');

function handleError(err, res, req){
    err.uid = appUtil.sessionUtil.getUserId(req.session, false);
    console.log(req.body);
    if(err.errorCode){
        //console.warn(err);
        if(err.validation){
            res.send(appUtil.getValidationError(err));
        }else{
            res.send(appUtil.getErrorResponse(err));}
        appUtil.logger.WARN(err, null);
    }else{
        res.send(appUtil.getErrorResponse(appUtil.exceptions.UNKNOWN_ERROR()));
        //console.log(err.stack);
        appUtil.logger.ERROR(err.stack, "uncatched");
    }
}

app.sendSuccess = function(response, result){
    response.send(appUtil.getSuccessResponse(result));
};

app.sendSuccess = function(response, result, isLevelCross){
    response.send(appUtil.getSuccessResponse(result, isLevelCross));
};

app.use(function(req, res, next){
    //throw appUtil.exceptions.VERSION_EXPIRED();
    var d = domain.create();
    d.on("error", function(err){
        handleError(err, res, req);
    });
    var _next = d.bind(next);
    _next();
});

var loginSignupController = require('./loginSignupController');
var postController = require('./postController');
var advertisementController = require('./advertisementController');
//user quota will be reset every day
service.userService.resetUserQuota();
//push notification after 15 minutes
//service.notificationsService.sendPushNotifications(appUtil.appConstants.NotificationInterval);
var apis = [];
apis = apis.concat(loginSignupController.apis);
apis = apis.concat(postController.apis);
apis = apis.concat(advertisementController.apis);

var swagger = require("swagger-node-express").createNew(app);

appUtil.swaggerUtil.convertToSwagger(swagger, apis);

swagger.configureDeclaration("pet", {
    description : "Operations about Pets",
    authorizations : ["oauth2"],
    produces: ["application/json"]
});

// set api info
swagger.setApiInfo({
    title: "ClickdCash APIs",
    description: "API documentations for Clickdcash",
    termsOfServiceUrl: "http://helloreverb.com/terms/",
    contact: "apiteam@wordnik.com",
    license: "Apache 2.0",
    licenseUrl: "http://www.apache.org/licenses/LICENSE-2.0.html"
});

swagger.configureSwaggerPaths("", "api-docs", "");
swagger.configure(appUtil.appConstants.APP_BASE_URL, "1.0.0");


app.get("/get/apis", function(req, res){
    res.send(JSON.stringify(apis));
});


app.use(function(err, req, res, next) {
    handleError(err, res, req);
});