var app = require('../app');
var service = require('../service');
var social = require('../social');
var model = require("../model");
var appUtil = require("../appUtil/appUtil");
var pointsProp = require('../property').getPointsProp();

var loginFb = {};
loginFb.nickname = "LoginFB";
loginFb.path = "/login/fb";
loginFb.method = "POST";
loginFb.params = [
    {name : "accessToken", type : "string", desc : "fb access token"},
    {name : "devId", type : "string", desc : "device id",required:false},
    {name : "pt", type : "int", desc : "platform .. 1 for android , 2 for ios" , required:false},
    {name : "referedBy", type : "string", desc : "userId who has refered to" , required:false}
];
loginFb.paramType = "form";
app.post(loginFb.path, function(req, res) {
	var fbToken = req.body.accessToken;
    var deviceId = req.body.devId;
    var pt = req.body.pt;
    var referedBy = req.body.referedBy;
    if(pt){
        model.db.PLATFORM.validate(pt);
    }
    validateDevice(deviceId, pt);
	social.getFBProfile(fbToken, function(fbUser) {
        var device = {id: deviceId, pt: pt, sessionId : req.session.id};
        if(referedBy){
            fbUser["referedBy"] = referedBy;
        }
        service.userService.CreateUpdateFbUser(fbUser, device, function(user) {
            appUtil.sessionUtil.loginUserIntoSession(req.session, user);
            app.sendSuccess(res, user);
        });
	});
});
var userDetails = {};
userDetails.nickname="userDetails";
userDetails.path="/user/details/:uid";
userDetails.paramType="path";
userDetails.method="GET";
userDetails.desc="user details";
userDetails.params = [ {name: "uid" , type:"string" , desc: "userId"}];
app.get(userDetails.path, function (req, res) {
    var user = appUtil.sessionUtil.getLoggedInUserDetail(req.session, true);
    var id = req.param('uid');
    service.userService.getUserById(id, function (resp) {
        app.sendSuccess(res, resp)
    });
});
var logout = {};
logout.nickname="logout";
logout.path="/logout";
logout.paramType="path";
logout.method="GET";
logout.desc="logout";
app.get(logout.path, function(req, res){
    var user = appUtil.sessionUtil.getLoggedInUserDetail(req.session, true);
    if(user){
        service.userService.removeUserDevice(req.session.user, req.session.id, function(resp) {
            appUtil.sessionUtil.clearSession(req);
            app.sendSuccess(res, {result : 1});
        });
    }else{
        app.sendSuccess(res, {result : 0});
    }
});

var getPoints = {};
getPoints.nickname="getPoints";
getPoints.path="/get/points";
getPoints.paramType="path";
getPoints.method="GET";
getPoints.desc="get Points";
app.get(getPoints.path, function(req, res){
    var user = appUtil.sessionUtil.getLoggedInUserDetail(req.session, true);
    app.sendSuccess(res, {result : pointsProp});
});

var getRanks = {};
getRanks.nickname="getRanks";
getRanks.path="/get/ranks";
getRanks.paramType="path";
getRanks.method="GET";
getRanks.desc="get Ranks";
app.get(getRanks.path, function(req, res){
    var user = appUtil.sessionUtil.getLoggedInUserDetail(req.session, true);
    service.userService.getUsersWithRanks(user._id, function(resp, rank, points) {
        app.sendSuccess(res, {result: resp, rank : rank, points : points});
    });
});

var getWinners = {};
getWinners.nickname="getWinners";
getWinners.path="/get/winners";
getWinners.paramType="path";
getWinners.method="GET";
getWinners.desc="get Winners";
getWinners.paramType = "query";
getWinners.params = [ {name: "day" , type:"number" , desc: "no of last days", required : false } ];
app.get(getWinners.path, function(req, res){
    var days = parseInt(req.param("day"));
    if(days) {
        var user = appUtil.sessionUtil.getLoggedInUserDetail(req.session, true);
        service.winnerService.getWinners(appUtil.getLastdays(days), function(resp) {
            app.sendSuccess(res, {result: resp});
        });
    }
    else{
        service.winnerService.getWinners([appUtil.getLastDate()], function(resp) {
            app.sendSuccess(res, {result: resp});
        });
    }
});

var addPoints = {};
addPoints.nickname="addPoints";
addPoints.path="/user/add/points/:event/";
addPoints.paramType="path";
addPoints.method="GET";
addPoints.desc="Add points";
addPoints.params = [ {name: "event" , type:"string" , desc: "event name"}];
app.get(addPoints.path, function (req, res) {
    var user = appUtil.sessionUtil.getLoggedInUserDetail(req.session, true);
    var event =  parseInt(req.param('event'));
    model.db.EVENT_TYPE.validate(event);
    var points = parseInt(pointsProp.event_points[event-1]);
    service.userService.addUserPoints(user._id, points, event, function (resp) {
        app.sendSuccess(res, resp);
    });
});

var getUserPoints = {};
getUserPoints.nickname="getUserPoints";
getUserPoints.path="/user/get/points";
getUserPoints.paramType="path";
getUserPoints.method="GET";
getUserPoints.desc="get points";
app.get(getUserPoints.path, function (req, res) {
    var user = appUtil.sessionUtil.getLoggedInUserDetail(req.session, true);
    service.userService.getUserPoints(user._id, function (resp) {
        app.sendSuccess(res, resp);
    });
});

var addRecharge = {};
addRecharge.nickname = "addRecharge";
addRecharge.path = "/add/recharge";
addRecharge.method = "POST";
addRecharge.params = [
    {name : "mobile", type : "string", desc : "mobile number"},
    {name : "operator", type : "string", desc : "operator",required:true},
    {name : "amount", type : "number", desc : "amount" , required:true}
];
addRecharge.paramType = "form";
app.post(addRecharge.path, function(req, res) {
    var user = appUtil.sessionUtil.getLoggedInUserDetail(req.session, true);
    var mobile = req.body.mobile;
    var amount = req.body.amount;
    var operator = req.body.operator;
    if(!mobile || !amount || !operator)
        throw appUtil.exceptions.PARAMETER_MISSING();
    service.userService.addRecharge(user._id, mobile, amount, operator, function(user) {
        app.sendSuccess(res, user);
    });
});

function validateDevice(deviceId, pt){
    if((deviceId && !pt) || (pt && !deviceId)){
        throw appUtil.exceptions.DEVICE_INFO();
    }
}

module.exports = {
    apis : [loginFb, logout, userDetails, getPoints, getRanks, getWinners, addPoints, getUserPoints, addRecharge]
};
