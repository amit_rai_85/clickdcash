var app = require('../app');
var service = require('../service');
var social = require('../social');
var model = require("../model");
var appUtil = require("../appUtil/appUtil");
var pointsProp = require('../property').getPointsProp();

var getAdds = {};
getAdds.nickname="getAdds";
getAdds.path="/get/adds";
getAdds.paramType="path";
getAdds.method="GET";
getAdds.desc="get adds";
app.get(getAdds.path, function(req, res){
    var user = appUtil.sessionUtil.getLoggedInUserDetail(req.session, true);
    service.advertisementService.getAdvertisement(user._id, function(resp) {
        app.sendSuccess(res, {result: resp});
    });
});

var getRewards = {};
getRewards.nickname="getRewards";
getRewards.path="/get/rewards/:userId/:addId";
getRewards.paramType="path";
getRewards.method="GET";
getRewards.desc="get adds";
getRewards.params = [ {name: "userId" , type:"string" , desc: "userId to get reward"},
    {name: "addId" , type:"string" , desc: "addId"}];
app.get(getRewards.path, function(req, res){
    var uid = req.param('userId');
    var aid = req.param('addId');
    service.advertisementService.addRewards(uid, aid, function(resp) {
        app.sendSuccess(res, {result: resp});
    });
});

var addAdds = {};
addAdds.nickname = "addAdds";
addAdds.path = "/add/advertisement";
addAdds.method = "POST";
addAdds.params = [
    {name : "AdType", type : "number", desc : "add type(1 for install etc)"},
    {name : "AdName", type : "string", desc : "add name"},
    {name : "Package", type : "string", desc : "package"},
    {name : "AdImage", type : "file", desc : "multi part image file for ad image"},
    {name : "AdIcon", type : "file", desc : "multi part image file for ad Icon"},
    {name : "vendorName", type : "string", desc : "vendor name"},
    {name : "AdSubTitle", type : "string", desc : "add subtitle"},
    {name : "AdDownText", type : "string", desc : "add down text"},
    {name : "AdExtraText", type : "string", desc : "add extra text"},
    {name : "AdTotalPrice", type : "number", desc : "add total price"},
    {name : "TotalImpression", type : "number", desc : "total impression"},
    {name : "vendorEmail", type : "string", desc : "vendor email id"},
    {name : "email", type : "string", desc : "email"},
    {name : "IndirectReward", type : "string", desc : "indirect reward"},
    {name : "DirectReward", type : "string", desc : "direct reward"},
    {name : "startDate", type : "string", desc : "start date of ad(dd/mm/yyyy)"},
    {name : "endDate", type : "string", desc : "end date of ad(dd/mm/yyyy)"},
    {name : "ButtonText", type : "string", desc : "Button Text"},
    {name : "ClickUrl", type : "string", desc : "add play store url"}
];
addAdds.paramType = "form";
app.post(addAdds.path, function(req, res) {
    var errorMessage = "";
    var icon;
    if(req.body.AdIconPicture) {
        icon = req.body.AdIconPicture;
    }else{
        icon = req.files.AdIcon;
    }
    if(!req.body.AdType || !req.body.AdName || !req.body.Package || !icon || !req.body.vendorName || !req.body.AdSubTitle
        || !req.body.AdDownText || !req.body.AdExtraText || !req.body.AdTotalPrice|| !req.body.TotalImpression
        || !req.body.vendorEmail|| !req.body.email || !req.body.IndirectReward || !req.body.DirectReward ||
        !req.body.startDate || !req.body.endDate || !req.body.ButtonText || !req.body.ClickUrl){
        throw appUtil.exceptions.PARAMETER_MISSING();
    }

    var addObj = {};
    addObj.AdType = req.body.AdType;
    addObj.AdName = req.body.AdName;
    addObj.Package = req.body.Package;
    var AdImage = req.files.AdImage;
    var AdIcon = req.files.AdIcon;
    addObj.vendorName = req.body.vendorName;
    addObj.AdSubTitle = req.body.AdSubTitle;
    addObj.AdDownText = req.body.AdDownText;
    addObj.AdExtraText = req.body.AdExtraText;
    addObj.AdTotalPrice = parseFloat(req.body.AdTotalPrice);
    addObj.TotalImpression = parseInt(req.body.TotalImpression);
    addObj.vendorEmail = req.body.vendorEmail;
    addObj.email = req.body.email;
    addObj.IndirectReward = parseFloat(req.body.IndirectReward);
    addObj.DirectReward = parseFloat(req.body.DirectReward);
    addObj.ButtonText = req.body.ButtonText;
    addObj.ClickUrl = req.body.ClickUrl;

    var from = req.body.startDate.split("/");
    addObj.startDate = new Date(from[2], from[1] - 1, from[0]);
    var to = req.body.endDate.split("/");
    addObj.endDate = new Date(to[2], to[1] - 1, to[0]);

    if (AdImage) {
        appUtil.fileStorage.upload(AdImage.path, AdImage.name, function (err, url) {
            if (err) {
                throw err;
            }
            var size = 40;
            var imageName = AdIcon.name;
            addObj.AdImage = url;
            appUtil.imageResizer.resize(AdIcon.path, appUtil.getUserHome() + "/ClickdCash/" + imageName + size, size, function (err) {
                if (err) {
                    appUtil.logger.logException(err, "imageResizer");
                    return;
                }
                appUtil.fileStorage.upload(appUtil.getUserHome() + "/ClickdCash/" + imageName + size, imageName + size, function (err, url) {
                    if (err) {
                        appUtil.logger.logException(err, "AWS");
                        return;
                    }
                    addObj.AdIcon = url;
                    service.advertisementService.insertIntoAdvertisement(addObj, function (result) {
                        app.sendSuccess(res, {result: result});
                    });
                });
            });
        });
    } else {
        var size = 40;
        var imageName = AdIcon.name;
        appUtil.imageResizer.resize(AdIcon.path, appUtil.getUserHome() + "/ClickdCash/" + imageName + size, size, function (err) {
            if (err) {
                appUtil.logger.logException(err, "imageResizer");
                return;
            }
            appUtil.fileStorage.upload(appUtil.getUserHome() + "/ClickdCash/" + imageName + size, imageName + size, function (err, url) {
                if (err) {
                    appUtil.logger.logException(err, "AWS");
                    return;
                }
                addObj.AdIcon = url;
                service.advertisementService.insertIntoAdvertisement(addObj, function (result) {
                    app.sendSuccess(res, {result: result});
                });
            });
        });
    }
});


module.exports = {
    apis : [getAdds, addAdds, getRewards]
};
