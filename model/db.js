var model = require("./model");
var ptsProp = require('../property').getPointsProp();

GENDER = {
		MALE : 1,
		FEMALE : 2,
		SECRET : 3,
		validate : function(val){
			if(val!=1 && val!=2 && val!=3){
				throw new model.AppException("gender001", "Invalid gender");
			}
		}
};

PERMISSION = {
		ALLOWED_TO_CONTACT : 1,
		NOT_ALLOWED_TO_CONTACT : 2,
		validate : function(val){
			if(val!=1 && val!=2){
				throw new model.AppException("perm001", "Invalid permission code");
			}
		}
};

USER_STATE = {
	ACTIVE : 1,
	INACTIVE : 2,
	validate : function(val){
		if(val!=1 && val!=2){
			throw new model.AppException("state001", "Invalid state code");
		}
	}
};

IS_ANONYMOUS = {
    YES : 1,
    NO : 0,
    validate : function(val){
        if(val!=1 && val!=0){
            throw new model.AppException("anonymous001", "Invalid anonymous code");
        }
    }
};

PLATFORM = {
    IOS : 1,
    ANDROID : 2,
    validate : function(val){
        if(val!=1 && val!=2){
            throw new model.AppException("platform001", "Invalid platform code");
        }
    }
};

NOTI_TYPE = {
    NEW : 0,
    LIKE : 1,
    COMMENT : 3
};


REPORT_TYPE = {
    REPORT_ABUSE : 1,
    validate : function(val){
        if(val!=1){
            throw new model.AppException("report001", "Invalid report type");
        }
    }
};

EVENT_TYPE = {
    FACEBOOK_SHARE : 1,
    WHATSSAPP_SHARE : 2,
    GOOGLE_SHARE : 3,
    validate : function(val){
        if(val!=1 && val!=2 && val!=3){
            throw new model.AppException("event001", "Invalid event code");
        }
    }
};

POLL_STATUS = {
    RUNNING : 1,
    ENDED : 2
};

function User(fid, eml, name, dp, gn, st, cat, referedBy){
	GENDER.validate(gn);
	USER_STATE.validate(st);
	this.fid = fid;
	this.eml = eml;
    this.name = name;
    this.dp = dp;
	this.gn = gn;
	this.st = st;
	this.cat = cat;
    this.pts = 0;
    this.dvcs = null;
    this.referedBy = referedBy;
	removeNullKeys(this);
}

function Post(msg, uid, pUrl, vUrl, ts, invu){
    this.msg = msg;
    this.uid = uid;
    this.likedBy = [];
    this.commentBy = [];
    this.pUrl = pUrl;
    this.vUrl = vUrl;
    this.ts = ts;
    this.invu = invu;
    this.lcnt = 0;
    this.ccnt = 0;
    this.rpt = null;
    removeNullKeys(this);
}
/**
 *
 * @param lg : longitude of location
 * @param lt : latitude of location
 * @constructor
 */
function Loc(lg, lt){
    this.lg = parseFloat(lg);
    this.lt = parseFloat(lt);
}

/**
 *
 * @param uid : userId of user
 * @param consecutiveVisit : number of consecutive visit
 * @param visitDay : last Visit Day
 * @constructor
 */
function Visits(uid, vcnt, vday){
    this.uid = uid;
    this.vcnt = vcnt;
    this.vday = vday;
    removeNullKeys(this);
}

/**
 *
 * @param invu : Array of json object({uid:uid, ts:ts, isv:true/false})
 * @param nt : notification type
 * @param nm : push notification message
 * @param pid : post id
 * @param ts : last update time stamp
 * @param po : post owner id
 * @param la : last actor on post
 * @constructor
 */
function Notifications(pid, nt, invu, nm, ts, po, la){
    this.pid = pid;
    this.nt = nt;
    this.invu = invu;
    this.nm = nm;
    this.iss = null;
    this.vb = null;
    this.ts = ts;
    this.po = po;
    this.la = la;
    removeNullKeys(this);
}

function Location(loc, city){
    this.loc = loc;
    this.city = city;
}

function Report(uid, rt, rat){
    this.uid = uid;
    this.rt = rt;
    this.rat = rat;
}

/**
 *
 * @param pq : Poll question
 * @param pa : options given for question
 * @param uid : creator id of poll
 * @param pst : poll start time
 * @param pd : poll duration
 * @constructor
 */
function Poll(pq, pa, uid, pst, pd, city, area, st){
    this.ques = pq;
    this.opt = pa;
    this.uid = uid;
    this.start = pst;
    this.dur = pd;
    this.end = pst + pd;
    this.city = city;
    this.area = area;
    this.st = st;
    this.invu = [];
    removeNullKeys(this);
}

PUSH_TYPE = {
    POST : 1,
    POLL : 2
};

function Winners(uid, date, points, name, img, rank){
    this.uid = uid;
    this.date = date;
    this.pts = points;
    this.name = name;
    this.img = img;
    this.rank = rank;
    removeNullKeys(this);
}


function Transactions(rt, amt, no, uid, ts, tid){
    this.rt = rt;
    this.amt = amt;
    this.no = no;
    this.uid = uid;
    this.ts = ts;
    this.tid = tid;
    removeNullKeys(this);
}


function Events(type, uid, ts){
    this.ev = type;
    this.uid = uid;
    this.ts = ts;
    removeNullKeys(this);
}

function Advertisement(AdType,AdName,Package,URL,AdIcon,AdImage,vendorName,AdSubTitle,AdDownText,AdExtraText,
                       AdTotalPrice,Active,TotalImpression,LeftImpression,vendorEmail,email,
                       IndirectReward, DirectReward, createdOn, startDate, endDate, ButtonText, ClickUrl){
    this.AdType = parseInt(AdType);
    this.AdName = AdName;
    this.Package = Package;
    this.URL = URL;
    this.AdIcon = AdIcon;
    this.AdImage = AdImage;
    this.vendorName = vendorName;
    this.AdSubTitle = AdSubTitle;
    this.AdDownText = AdDownText;
    this.AdExtraText = AdExtraText;
    this.AdTotalPrice = parseFloat(AdTotalPrice);
    this.Active = Active;
    this.TotalImpression = parseInt(TotalImpression);
    this.LeftImpression = parseInt(LeftImpression);
    this.vendorEmail = vendorEmail;
    this.email = email;
    if(parseFloat(IndirectReward)) {
        this.IndirectReward = parseFloat(IndirectReward);
    }
    else{
        this.IndirectReward = 0;
    }
    if(parseFloat(DirectReward)) {
        this.DirectReward = parseFloat(DirectReward);
    }
    else{
        this.DirectReward = 0;
    }
    this.createdOn = createdOn;
    this.startDate = startDate;
    this.endDate = endDate;
    this.ButtonText = ButtonText;
    this.ClickUrl = ClickUrl;
    removeNullKeys(this);
}


module.exports = {
    GENDER : GENDER,
    PERMISSION : PERMISSION,
    USER_STATE : USER_STATE,
    IS_ANONYMOUS : IS_ANONYMOUS,
    User : User,
    Loc : Loc,
    Post : Post,
    Visits : Visits,
    PLATFORM : PLATFORM,
    Notifications : Notifications,
    NOTI_TYPE : NOTI_TYPE,
    Location: Location,
    REPORT_TYPE: REPORT_TYPE,
    Report: Report,
    Poll: Poll,
    POLL_STATUS : POLL_STATUS,
    PUSH_TYPE : PUSH_TYPE,
    Winners : Winners,
    Transactions : Transactions,
    Events : Events,
    EVENT_TYPE : EVENT_TYPE,
    Advertisement : Advertisement
};

function removeNullKeys(ob){
	for(var i in ob){
		if(ob[i]==null || ob[i]==undefined){
			delete ob[i];
		}
	}
}