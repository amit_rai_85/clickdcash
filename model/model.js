exports.AppException = function(errorCode, errorMessage, error){
	this.errorCode = errorCode;
	this.errorMessage = errorMessage;
	this.error = error;
};
exports.validationAppException = function(errorCode, errorMessage, error){
	this.errorCode = errorCode;
	this.errorMessage = errorMessage;
	this.error = error;
	this.validation = true;
};
exports.APIResponse = function(statusCode, result){
	this.statusCode = statusCode;
	this.result = result;
};

exports.db = require('./db');